<?php
session_start();
if (isset($_SESSION["shipper_id"]))   
{
    $role = $_SESSION["role"];
    $shipper_id = $_SESSION["shipper_id"];
    $full_name = $_SESSION["full_name"];
    $email_adr = $_SESSION["email_adr"];

    if ($role === 'shipper')
    {
    ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>SOTA FREIGHTS - Profile Update</title>

    <!-- vendor css -->
    <link href="../lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
		<link href="../lib/ionicons/css/ionicons.min.css" rel="stylesheet">
		<link href="../lib/highlightjs/styles/github.css" rel="stylesheet">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="../css/bracket.css">
    <link rel="stylesheet" href="../css/local.css">
  </head>

  <body>

  <?php require 'shipmenu.php';?>

    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pagetitle">
        <i class="icon ion-ios-gear-outline"></i>
        <div>
          <h4>Profile Update</h4>
          <p class="mg-b-0">SOTA FREIGHT LTD</p>
        </div>
      </div><!-- d-flex -->
      <div class="br-pagebody">
        <div class="br-section-wrapper">
       <div> <!-- Table Begin -->
       <fieldset>
            <legend>User Profile Information</legend>
       <?php
require_once 'dbconn.inc.php';

$sql = "SELECT * FROM shippers WHERE shipper_id = '$shipper_id'";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
?>
      <div class="table-wrapper">
            <table class="table table-bordered table-primary">
        <thead>
        <tr>
                  <th class="wd-15p">User ID</th>
                  <th class="wd-15p">Fullname</th>
                  <th class="wd-15p">Email Address</th>
                  <th class="wd-20p">Phone Number</th>
                  <th class="wd-15p">Company Name</th>
                  <th class="wd-10p">Company Address</th>
                  <th class="wd-25p">Zip Code</th>
                  <th class="wd-25p">Tax ID</th>
                </tr>
        </thead>
        <tbody>
            <?php
    while($row = $result->fetch_assoc()) 
    {
    ?> 
        <tr>
            <td><?php echo $row["shipper_id"];?></td>
              <td><?php echo $row["full_name"];?></td>
              <td><?php echo $row["email_adr"];?></td>
              <td><?php echo $row["tel_num"];?></td>
              <td><?php echo $row["comp_name"];?></td>
              <td><?php echo $row["comp_adr"];?></td>
              <td><?php echo $row["zipcode"];?></td>
              <td><?php echo $row["tax_id"];?></td>
            </tr>
            <?php } ?>
        </tbody>
      </table>
      <?php   
}
else 
{
echo "No Record";
}
$conn->close();
?>
      </div><!-- table-wrapper -->
       </fieldset>
       </div><!-- Table End -->
          <h6 class="br-section-label">Profile Update</h6>
          <p class="br-section-text">Please fill the form below to update user profile.</p>
          <?php 
              if (isset($_GET['error']))
              {   
                  if($_GET['error'] == 'stmtfailed')
                  {
                    echo '<h5 class="blinking tx-inverse tx-center">Connection Error, Please try again!</h5>';
                  }
                  if($_GET['error'] == 'none')
                  {
                    echo '<h5 class="blinking tx-inverse tx-center">Profile Updated Successfully!</h5>';
                  }
              }
          ?>
     
          <div class="form-layout form-layout-1">
            <form method="POST" action = "/includes/profile-shipper-update.inc.php">
            <div class="row mg-b-25">
              <input type="hidden" name="shipper_id" value="<?php echo $shipper_id; ?>">
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Enter Fullname: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="full_name" required>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Enter Phone Number: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="tel_num" required>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Enter Company Name: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="comp_name" required>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Enter Company Address: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="comp_adr" required>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Enter Zip Code: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="zipcode" required>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Enter Tax ID: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="tax_id" required>
                </div>
              </div><!-- col-4 -->
            </div><!-- row -->
            <div class="form-layout-footer">
              <button class="btn btn-info" name="submit">Update Profile</button>
              <button class="btn btn-secondary">Cancel</button>
            </div><!-- form-layout-footer -->
            </form>
          </div><!-- form-layout -->                 
        </div><!-- br-section-wrapper -->
      </div><!-- br-pagebody -->
      <footer class="br-footer">
        <div class="footer-left">
        <div class="mg-b-2">Copyright &copy; 2020. SOTA FREIGHT LTD. All Rights Reserved.</div>
          <div>Attentively and carefully Developed by <a href="https://solucean.ng">SOLUCEAN LTD</a></div>
        </div>
        <div class="footer-right d-flex align-items-center">
          <span class="tx-uppercase mg-r-10">Share:</span>
          <a target="_blank" class="pd-x-5" href="https://facebook.com/sotafreight"><i class="fab fa-facebook tx-20"></i></a>
          <a target="_blank" class="pd-x-5" href="https://twitter.com/sotafreight"><i class="fab fa-twitter tx-20"></i></a>
        </div>
      </footer>
    </div><!-- br-mainpanel -->
    <!-- ########## END: MAIN PANEL ########## -->

    <script src="../lib/jquery/jquery.min.js"></script>
    <script src="../lib/jquery-ui/ui/widgets/datepicker.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="../lib/moment/min/moment.min.js"></script>
    <script src="../lib/peity/jquery.peity.min.js"></script>
    <script src="../lib/highlightjs/highlight.pack.min.js"></script>
    <script src="../lib/select2/js/select2.min.js"></script>

    <script src="../js/bracket.js"></script>
    <script>
      $(function(){
        'use strict'

        $('.form-layout .form-control').on('focusin', function(){
          $(this).closest('.form-group').addClass('form-group-active');
        });

        $('.form-layout .form-control').on('focusout', function(){
          $(this).closest('.form-group').removeClass('form-group-active');
        });

        // Select2
        $('#select2-a, #select2-b').select2({
          minimumResultsForSearch: Infinity
        });

        $('#select2-a').on('select2:opening', function (e) {
          $(this).closest('.form-group').addClass('form-group-active');
        });

        $('#select2-a').on('select2:closing', function (e) {
          $(this).closest('.form-group').removeClass('form-group-active');
        });

      });
    </script>
  </body>
</html>

<?php
    exit();
    }
        elseif($role !== "shipper")
        {
            header("Location: /ship/page-not-found.html");
            exit();
        }
}

if (!isset($_SESSION["shipper_id"]))
{
    header("Location: /ship/shipper-signin.php");
        exit();
}
?>