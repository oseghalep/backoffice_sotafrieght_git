<?php
session_start();
if (isset($_SESSION["shipper_id"]))   
{
    $role = $_SESSION["role"];
    $shipper_id = $_SESSION["shipper_id"];
    $full_name = $_SESSION["full_name"];
    $email_adr = $_SESSION["email_adr"];

    if ($role === 'shipper')
    {
    ?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="SOTA FREIGHTS LTD">
    <meta name="author" content="SOTA FREIGHTS">

    <title>SOTA FREIGHTS LTD</title>

    <!-- vendor css -->
    <link href="../lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="../lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="../lib/rickshaw/rickshaw.min.css" rel="stylesheet">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="../css/bracket.css">
  </head>

  <body>
    <!-- <span>[</span>bracket <i>plus</i><span>]</span></a>-->
  
    <!-- ########## START: LEFT PANEL ########## -->
    <div class="br-logo"><a href="index.php"><img src="../img/logo.png" alt="Truckload analysis Chat" style="float:left"></div>
    <div class="br-sideleft sideleft-scrollbar">
      <label class="sidebar-label pd-x-10 mg-t-20 op-3">Navigation</label>
      <ul class="br-sideleft-menu">
        <li class="br-menu-item">
          <a href="index.php" class="br-menu-link active">
            <i class="menu-item-icon icon ion-ios-home-outline tx-24"></i>
            <span class="menu-item-label">Dashboard</span>
          </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
        <li class="br-menu-item">
          <a href="shipment.php" class="br-menu-link">
            <i class="menu-item-icon icon ion-ios-paper-outline tx-22"></i>
            <span class="menu-item-label">Create Shipment</span>
          </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
        <li class="br-menu-item">
          <a href="shipmentrec.php" class="br-menu-link">
            <i class="menu-item-icon icon ion-ios-book-outline tx-22"></i>
            <span class="menu-item-label">Shipment Record</span>
          </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
        <li class="br-menu-item">
          <a href="profile-shipper-update.php" class="br-menu-link">
            <i class="menu-item-icon icon ion-ios-list-outline tx-22"></i>
            <span class="menu-item-label">Edit Profile</span>
          </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
        <li class="br-menu-item">
          <a href="password-change.php" class="br-menu-link">
            <i class="menu-item-icon icon ion-ios-list-outline tx-22"></i>
            <span class="menu-item-label">Update Password</span>
          </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
        <li class="br-menu-item">
          <a href="/includes/logout.inc.php" class="br-menu-link">
            <i class="menu-item-icon icon ion-ios-list-outline tx-22"></i>
            <span class="menu-item-label">Signout</span>
          </a><!-- br-menu-link -->
        </li><!-- br-menu-item -->
      </ul><!-- br-sideleft-menu -->

      <label class="sidebar-label pd-x-10 mg-t-25 mg-b-20 tx-info">Information Summary</label>

      <div class="info-list">
        <div class="info-list-item">
          <div>
            <p class="info-list-label">Account Name</p>
            <h5 class="info-list-amount"><?php echo $full_name; ?></h5>
          </div>
          
        </div><!-- info-list-item -->

        <div class="info-list-item">
          <div>
            <p class="info-list-label">Account Email</p>
            <h5 class="info-list-amount"><?php echo $email_adr; ?></h5>
          </div>
        </div><!-- info-list-item -->
      </div><!-- info-list -->

      <br>
    </div><!-- br-sideleft -->
    <!-- ########## END: LEFT PANEL ########## -->

    <!-- ########## START: HEAD PANEL ########## -->
    <div class="br-header">
      <div class="br-header-left">
        <div class="navicon-left hidden-md-down"><a id="btnLeftMenu" href=""><i class="icon ion-navicon-round"></i></a></div>
        <div class="navicon-left hidden-lg-up"><a id="btnLeftMenuMobile" href=""><i class="icon ion-navicon-round"></i></a></div>
        <div class="input-group hidden-xs-down wd-170 transition">
          <input id="searchbox" type="text" class="form-control" placeholder="Search">
          <span class="input-group-btn">
            <button class="btn btn-secondary" type="button"><i class="fas fa-search"></i></button>
          </span>
        </div><!-- input-group -->
      </div><!-- br-header-left -->
      <div class="br-header-right">
        <nav class="nav">
          <div class="dropdown">
          </div><!-- dropdown -->
          <div class="dropdown">
            
          </div><!-- dropdown -->
          <div class="dropdown">
            <a href="" class="nav-link nav-link-profile" data-toggle="dropdown">
              <span class="logged-name hidden-md-down"><?php echo $shipper_id; ?></span>
              <img src="../img/favicon.png" class="wd-32 rounded-circle" alt="">
              <span class="square-10 bg-success"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-header wd-250">
              <div class="tx-center">
                <a href=""><img src="../img/favicon.png" class="wd-80 rounded-circle" alt=""></a>
                <h6 class="logged-fullname"><?php echo $full_name; ?></h6>
                <p><?php echo $email_adr; ?></p>
              </div>
              <hr>
              <ul class="list-unstyled user-profile-nav">
                <li><a href="profile-shipper-update.php"><i class="icon ion-ios-person"></i> Edit Profile</a></li>
                <hr>
              <ul class="list-unstyled user-profile-nav">
                <li><a href="password-change.php"><i class="icon ion-ios-person"></i>Update Password</a></li>
                <hr>
                <li><a href="/includes/logout.inc.php"><i class="icon ion-power"></i> Sign Out</a></li>
              </ul>
            </div><!-- dropdown-menu -->
          </div><!-- dropdown -->
        </nav>
        
            <!-- end: if statement -->
          </a>
        </div><!-- navicon-right -->
      </div><!-- br-header-right -->
    </div><!-- br-header -->
    <!-- ########## END: HEAD PANEL ########## -->


    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pagetitle">
        <i class="icon ion-ios-home-outline"></i>
        <div>
          <h4>Dashboard</h4>
          <p class="mg-b-0">Simplifying your freight & logistics needs with a personal approach.</p>
        </div>
      </div>

      <div class="br-pagebody">
        <div class="row row-sm">
          <div class="col-sm-6 col-xl-3">
            <div class="bg-info rounded overflow-hidden">
              <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                <i class="ion ion-earth tx-60 lh-0 tx-white op-7"></i>
                <div class="mg-l-20">
                  <p class="tx-10 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-10">Transportation</p>
                  <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1">Over 70%</p>
                  <span class="tx-11 tx-roboto tx-white-8">Goods carried by trucks</span>
                </div>
              </div>
              <div id="ch1" class="ht-50 tr-y-1"></div>
            </div>
          </div><!-- col-3 -->
          <div class="col-sm-6 col-xl-3 mg-t-20 mg-sm-t-0">
            <div class="bg-purple rounded overflow-hidden">
              <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                <i class="ion ion-bag tx-60 lh-0 tx-white op-7"></i>
                <div class="mg-l-20">
                  <p class="tx-10 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-10">Freight 2020</p>
                  <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1">Over 10.8</p>
                  <span class="tx-11 tx-roboto tx-white-8">Billion tons of Freight</span>
                </div>
              </div>
              <div id="ch3" class="ht-50 tr-y-1"></div>
            </div>
          </div><!-- col-3 -->
          <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
            <div class="bg-teal rounded overflow-hidden">
              <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                <i class="ion ion-monitor tx-60 lh-0 tx-white op-7"></i>
                <div class="mg-l-20">
                  <p class="tx-10 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-10">Projection 2021</p>
                  <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1">4.9%</p>
                  <span class="tx-11 tx-roboto tx-white-8">Expected Growth Rate</span>
                </div>
              </div>
              <div id="ch2" class="ht-50 tr-y-1"></div>
            </div>
          </div><!-- col-3 -->
          <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
            <div class="bg-primary rounded overflow-hidden">
              <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                <i class="ion ion-clock tx-60 lh-0 tx-white op-7"></i>
                <div class="mg-l-20">
                 <p class="tx-10 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-10">Current Time</p>
                  <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1"><?php echo $timestamp = date('H:i:s');?></p>
                  <span class="tx-11 tx-roboto tx-white-8">Current average time</span>
                </div>
              </div>
              <div id="ch4" class="ht-50 tr-y-1"></div>
            </div>
          </div><!-- col-3 -->
        </div><!-- row -->

        <div class="row row-sm mg-t-20">
          <div class="col-lg-8">
            <div class="card bd-0 shadow-base">
              <div class="d-md-flex justify-content-between pd-25">
                <div>
                  <h6 class="tx-13 tx-uppercase tx-inverse tx-semibold tx-spacing-1">Truckload Linehaul Index</h6>
                  <p>per-mile linehaul rates</p>
                </div>
                <div class="d-sm-flex">
                  <div>
                    <p class="mg-b-5 tx-uppercase tx-10 tx-mont tx-semibold">Start Period</p>
                    <h4 class="tx-lato tx-inverse tx-bold mg-b-0">Sept 2005</h4>
                    <span class="tx-12 tx-success tx-roboto">2.7% increased</span>
                  </div>
                  <div class="bd-sm-l pd-sm-l-20 mg-sm-l-20 mg-t-20 mg-sm-t-0">
                    <p class="mg-b-5 tx-uppercase tx-10 tx-mont tx-semibold">End Period</p>
                    <h4 class="tx-lato tx-inverse tx-bold mg-b-0">Sept 2020</h4>
                    <span class="tx-12 tx-danger tx-roboto">3.4% decreased</span>
                  </div>
                  <div class="bd-sm-l pd-sm-l-20 mg-sm-l-20 mg-t-20 mg-sm-t-0">
                    <p class="mg-b-5 tx-uppercase tx-10 tx-mont tx-semibold">Source</p>
                    <h4 class="tx-lato tx-inverse tx-bold mg-b-0">Sept 2020</h4>
                    <span class="tx-12 tx-success tx-roboto">Cass IS, Inc.</span>
                  </div>
                </div><!-- d-flex -->
              </div><!-- d-flex -->
              <img src="../img/Chart10.png" alt="Truckload analysis Chat" style="float:left">
              <div class="pd-l-25 pd-r-15 pd-b-25">
             
                <!--<div id="ch5" class="ht-250 ht-sm-300"></div> -->
              </div>
            </div><!-- card -->

            <div class="card bd-0 shadow-base pd-25 mg-t-20">
              <div class="d-md-flex justify-content-between pd-25">
                <div>
                  <h6 class="tx-13 tx-uppercase tx-inverse tx-semibold tx-spacing-1">Average Cost per shipment</h6>
                  <p>Index Y/Y</p>
                </div>
                <div class="d-sm-flex">
                  <div>
                    <p class="mg-b-5 tx-uppercase tx-10 tx-mont tx-semibold">Start Period</p>
                    <h4 class="tx-lato tx-inverse tx-bold mg-b-0">Sept 2008</h4>
                    <span class="tx-12 tx-success tx-roboto">2.7% increased</span>
                  </div>
                  <div class="bd-sm-l pd-sm-l-20 mg-sm-l-20 mg-t-20 mg-sm-t-0">
                    <p class="mg-b-5 tx-uppercase tx-10 tx-mont tx-semibold">End Period</p>
                    <h4 class="tx-lato tx-inverse tx-bold mg-b-0">Sept 2020</h4>
                    <span class="tx-12 tx-danger tx-roboto">3.4% decreased</span>
                  </div>
                  <div class="bd-sm-l pd-sm-l-20 mg-sm-l-20 mg-t-20 mg-sm-t-0">
                    <p class="mg-b-5 tx-uppercase tx-10 tx-mont tx-semibold">Source</p>
                    <h4 class="tx-lato tx-inverse tx-bold mg-b-0">Sept 2020</h4>
                    <span class="tx-12 tx-success tx-roboto">Cass IS, Inc.</span>
                  </div>
                </div><!-- d-flex -->
              </div><!-- d-flex -->
              <img src="../img/Chart8.png" alt="Truckload analysis Chat" style="float:left">
              <div class="pd-l-25 pd-r-15 pd-b-25">
             
                <!--<div id="ch5" class="ht-250 ht-sm-300"></div> -->
              </div>
            </div><!-- card -->
          </div><!-- col-8 -->
          <div class="col-lg-4 mg-t-20 mg-lg-t-0">

            <div class="card shadow-base bd-0 overflow-hidden">
              <div class="pd-x-25 pd-t-25">
                <h6 class="tx-13 tx-uppercase tx-inverse tx-semibold tx-spacing-1 mg-b-20">Annual High and Low Points in Freight Transportation Services Index, 2009-2020</h6>
                
              </div><!-- pd-x-25 -->
              
              <img src="../img/table1.png" alt="Truckload analysis Chat">
            </div><!-- card -->

            <div class="card shadow-base bd-0 mg-t-20">
              <div class="pd-x-25 pd-t-25">
                <h6 class="tx-13 tx-uppercase tx-inverse tx-semibold tx-spacing-1 mg-b-20">Freight Transportation Services Index from Year-to-Year Percent Change in the May Freight TSI</h6>
                
              </div><!-- pd-x-25 -->
              
              <img src="../img/table2.png" alt="Truckload analysis Chat">
            </div><!-- card -->

        </div><!-- col-4 -->
        </div><!-- row -->

      </div><!-- br-pagebody -->
      <footer class="br-footer">
        <div class="footer-left">
          <div class="mg-b-2">Copyright &copy; 2020. SOTA FREIGHT LTD. All Rights Reserved.</div>
          <div>Attentively and carefully Developed by <a href="https://solucean.ng">SOLUCEAN LTD</a></div>
        </div>
        <div class="footer-right d-flex align-items-center">
          <span class="tx-uppercase mg-r-10">Share:</span>
          <a target="_blank" class="pd-x-5" href="https://facebook.com/sotafreight"><i class="fab fa-facebook tx-20"></i></a>
          <a target="_blank" class="pd-x-5" href="https://twitter.com/sotafreight"><i class="fab fa-twitter tx-20"></i></a>
        </div>
      </footer>
    </div><!-- br-mainpanel -->
    <!-- ########## END: MAIN PANEL ########## -->

    <script src="../lib/jquery/jquery.min.js"></script>
    <script src="../lib/jquery-ui/ui/widgets/datepicker.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="../lib/moment/min/moment.min.js"></script>
    <script src="../lib/peity/jquery.peity.min.js"></script>
    <script src="../lib/rickshaw/vendor/d3.min.js"></script>
    <script src="../lib/rickshaw/vendor/d3.layout.min.js"></script>
    <script src="../lib/rickshaw/rickshaw.min.js"></script>
    <script src="../lib/jquery.flot/jquery.flot.js"></script>
    <script src="../lib/jquery.flot/jquery.flot.resize.js"></script>
    <script src="../lib/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../lib/jquery-sparkline/jquery.sparkline.min.js"></script>
    <script src="../lib/echarts/echarts.min.js"></script>
    <script src="../lib/select2/js/select2.full.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyAq8o5-8Y5pudbJMJtDFzb8aHiWJufa5fg"></script>
    <script src="../lib/gmaps/gmaps.min.js"></script>

    <script src="../js/bracket.js"></script>
    <script src="../js/map.shiftworker.js"></script>
    <script src="../js/ResizeSensor.js"></script>
    <script src="../js/dashboard.js"></script>
    <script>
      $(function(){
        'use strict'

        // FOR DEMO ONLY
        // menu collapsed by default during first page load or refresh with screen
        // having a size between 992px and 1299px. This is intended on this page only
        // for better viewing of widgets demo.
        $(window).resize(function(){
          minimizeMenu();
        });

        minimizeMenu();

        function minimizeMenu() {
          if(window.matchMedia('(min-width: 992px)').matches && window.matchMedia('(max-width: 1299px)').matches) {
            // show only the icons and hide left menu label by default
            $('.menu-item-label,.menu-item-arrow').addClass('op-lg-0-force d-lg-none');
            $('body').addClass('collapsed-menu');
            $('.show-sub + .br-menu-sub').slideUp();
          } else if(window.matchMedia('(min-width: 1300px)').matches && !$('body').hasClass('collapsed-menu')) {
            $('.menu-item-label,.menu-item-arrow').removeClass('op-lg-0-force d-lg-none');
            $('body').removeClass('collapsed-menu');
            $('.show-sub + .br-menu-sub').slideDown();
          }
        }
      });
    </script>
  </body>
</html>


<?php
    exit();
    }
        elseif($role !== "shipper")
        {
            header("Location: /ship/page-not-found.html");
            exit();
        }
}

if (!isset($_SESSION["shipper_id"]))
{
    header("Location: /ship/shipper-signin.php");
        exit();
}
?>