<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="SOTA Freights LTD.">
    <meta name="author" content="SOTA Freights">

    <title>SOTA Freights LTD</title>

    <!-- vendor css -->
    <link href="../lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="../lib/ionicons/css/ionicons.min.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="../css/bracket.css">
  </head>

  <body>

    <div class="row no-gutters flex-row-reverse ht-100v">
      <div class="col-md-6 bg-gray-200 d-flex align-items-center justify-content-center">
        <div class="login-wrapper wd-250 wd-xl-350 mg-y-30">
          <h4 class="tx-inverse tx-center">Create New Password</h4>
          <p class="tx-center mg-b-60">Welcome back my friend! Now let's give you a new Password.</p>
          <?php 
              if (isset($_GET['error']))
              {
                  if($_GET['error'] == 'empty')
                  {
                      echo '<h6 class="tx-inverse tx-center">Please Enter Password Below!</h6>';
                  }
                  if($_GET['error'] == 'pwdnotsame')
                  {
                    echo '<h6 class="tx-inverse tx-center">Password Values Different!</h6>';
                  }
                  if($_GET['error'] == 'stmterr')
                  {
                    echo '<h6 class="tx-inverse tx-center">Network Error!</h6>';
                  }
                  if($_GET['error'] == 'badrq')
                  {
                    echo '<h6 class="tx-inverse tx-center">Please Resubmit your request!</h6>';
                  }
              }
          ?>

          <?php
          $token = $_GET["token"];


          if (empty($token)) 
          {
            header("Location: /ship/password-reset.php?error=badrq");
            exit();
          }
          else
          {
                ?>

          <form action="/includes/reset-password.inc.php" method="POST">
          <div class="form-group">
            <input type="password" name="pwd" class="form-control" placeholder="Enter a New Password">
          </div><!-- form-group -->
          <div class="form-group">
            <input type="password" name="pwdrepeat" class="form-control" placeholder="Repeat New Password">
          </div><!-- form-group -->
          <button type="submit" name="submit" class="btn btn-info btn-block">Reset Password</button>
        </form>
        <?php
}
?>
        </div><!-- login-wrapper -->
      </div><!-- col -->
      <div class="col-md-6 bg-br-primary d-flex align-items-center justify-content-center">
        <div class="wd-250 wd-xl-450 mg-y-30">
          <div class="signin-logo tx-28 tx-bold tx-white"><img src="../img/logo21.png" alt="Truckload analysis Chat"></div>
          <div class="tx-white mg-b-60"><h6>Freight Services For Perfectionist</h6></div>

          <h5 class="tx-white">Why SOTA Freights?</h5>
          <p class="tx-white-6">We offer distinct Professional Freights services, quick response time, well maintained trucks and highly trained drivers.</p>
          <p class="tx-white-6 mg-b-60">We are focused on building long lasting business relationship with our customers through quality service delivery and customer satisfaction.</p>
          <a href="https://sotafreight.com/" class="btn btn-outline-light bd bd-white bd-2 tx-white pd-x-25 tx-uppercase tx-12 tx-spacing-2 tx-medium">SOTA FREIGHT-Home</a>
        </div><!-- wd-500 -->
      </div>
    </div><!-- row -->

    <script src="../lib/jquery/jquery.min.js"></script>
    <script src="../lib/jquery-ui/ui/widgets/datepicker.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>
</html>
