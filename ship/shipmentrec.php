<?php
session_start();
if (isset($_SESSION["shipper_id"]))   
{
    $role = $_SESSION["role"];
    $shipper_id = $_SESSION["shipper_id"];
    $full_name = $_SESSION["full_name"];
    $email_adr = $_SESSION["email_adr"];

    if ($role === 'shipper')
    {
    ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Shipper Account - SOTA FREIGHTS LTD</title>

    <!-- vendor css -->
    <link href="../lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
		<link href="../lib/ionicons/css/ionicons.min.css" rel="stylesheet">
		<link href="../lib/highlightjs/styles/github.css" rel="stylesheet">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet">
    <link href="../lib/datatables.net-dt/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="../lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="../css/bracket.css">
    <link rel="stylesheet" href="../css/local.css">
  </head>

  <body>

  <?php require 'shipmenu.php';?>

    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pagetitle">
        <i class="icon icon ion-ios-bookmarks-outline"></i>
        <div>
          <h4>Shipment Record</h4>
          <p class="mg-b-0">SOTA FREIGHTS SHIPPER</p>
        </div>
      </div><!-- d-flex -->

      <div class="br-pagebody">
        <div class="br-section-wrapper">
          <h6 class="br-section-label">Shipment DataTable</h6>
          <p class="br-section-text">Searching, ordering and paging has been implemented to this table.</p>
<?php
require_once 'dbconn.inc.php';

$sql = "SELECT * FROM shipment WHERE shipper_id = '$shipper_id'";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
?>
          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-15p">Shipment ID</th>
                  <th class="wd-15p">Pickup Address</th>
                  <th class="wd-15p">Delivery Address</th>
                  <th class="wd-20p">Item Description</th>
                  <th class="wd-15p">Item Weight</th>
                  <th class="wd-10p">Item Size</th>
                  <th class="wd-25p">Item Quantity</th>
                  <th class="wd-25p">Comment</th>
                </tr>
</thead>
              <tbody>
              <?php
while($row = $result->fetch_assoc()) 
{
?>
            <tr>
            <td><?php echo $row["shipment_id"];?></td>
              <td><?php echo $row["pickup_adr"];?></td>
              <td><?php echo $row["delivery_adr"];?></td>
              <td><?php echo $row["item_desc"];?></td>
              <td><?php echo $row["item_weight"];?></td>
              <td><?php echo $row["item_size"];?></td>
              <td><?php echo $row["item_quantity"];?></td>
              <td><?php echo $row["comment"];?></td>
            </tr>
            <?php } ?>
              </tbody>

            </table>
<?php   
}
else 
{
echo "No Shipment Record";
}
$conn->close();
?>
          </div><!-- table-wrapper -->

        </div><!-- br-section-wrapper -->
      </div><!-- br-pagebody -->
      <footer class="br-footer">
        <div class="footer-left">
        <div class="mg-b-2">Copyright &copy; 2020. SOTA FREIGHT LTD. All Rights Reserved.</div>
          <div>Attentively and carefully Developed by <a href="https://solucean.ng">SOLUCEAN LTD</a></div>
        </div>
        <div class="footer-right d-flex align-items-center">
          <span class="tx-uppercase mg-r-10">Share:</span>
          <a target="_blank" class="pd-x-5" href="https://facebook.com/sotafreight"><i class="fab fa-facebook tx-20"></i></a>
          <a target="_blank" class="pd-x-5" href="https://twitter.com/sotafreight"><i class="fab fa-twitter tx-20"></i></a>
        </div>
      </footer>
    </div><!-- br-mainpanel -->
    <!-- ########## END: MAIN PANEL ########## -->

    <script src="../lib/jquery/jquery.min.js"></script>
    <script src="../lib/jquery-ui/ui/widgets/datepicker.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="../lib/moment/min/moment.min.js"></script>
    <script src="../lib/peity/jquery.peity.min.js"></script>
    <script src="../lib/highlightjs/highlight.pack.min.js"></script>
    <script src="../lib/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../lib/datatables.net-dt/js/dataTables.dataTables.min.js"></script>
    <script src="../lib/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js"></script>
    <script src="../lib/select2/js/select2.min.js"></script>

    <script src="../js/bracket.js"></script>
    <script>
      $(function(){
        'use strict';

        $('#datatable1').DataTable({
          responsive: true,
          language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page',
          }
        });

        $('#datatable2').DataTable({
          bLengthChange: false,
          searching: false,
          responsive: true
        });

        // Select2
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

      });
    </script>
  </body>
</html>


<?php
    exit();
    }
        elseif($role !== "shipper")
        {
            header("Location: /ship/page-not-found.html");
            exit();
        }
}

if (!isset($_SESSION["shipper_id"]))
{
    header("Location: /ship/shipper-signin.php");
        exit();
}
?>
