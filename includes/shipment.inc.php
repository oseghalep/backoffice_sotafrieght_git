<?php
require_once 'config.ship.inc.php';
require 'dbconn.inc.php';
?>
<?php
if (isset($_POST['submit']))
{
    $shipper_id = $_POST['shipper_id'];
    $pickup_adr = $_POST['pickup_adr'];
    $delivery_adr = $_POST['delivery_adr'];
    $item_desc = $_POST['item_desc'];
    $item_weight = $_POST['item_weight'];
    $item_size = $_POST['item_size'];
    $item_quantity = $_POST['item_quantity'];
    $comment = $_POST['comment'];  

    

    createShipment($conn, $shipper_id, $pickup_adr, $delivery_adr, $item_desc, $item_weight, $item_size, $item_quantity, $comment);
}
else
{
    header("Location: /ship/shipment.php");
    exit();
}
