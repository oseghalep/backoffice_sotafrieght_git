<?php

if (isset($_POST["submit"]))
{
    $token = $_POST["token"];
    $pwd = $_POST["pwd"];
    $pwdrepeat = $_POST["pwdrepeat"];

    if (empty($pwd) || empty($pwdrepeat))
    {
        header ("Location: /driver/create-new-password.php?error=empty");
        exit();
    }
    else if ($pwd != $pwdrepeat)
    {
        header ("Location: /driver/create-new-password.php?error=pwdnotsame");
        exit();
    }

    $currentDate = date("U");

    require 'dbconn.inc.php';

  

    $sql = "SELECT * FROM pwdreset WHERE pwdResetToken = ? AND pwdResetExpires >= ?;";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql))
    {
        header ("Location: /driver/create-new-password.php?error=stmterr");
        exit();
    }
    else
    {
        mysqli_stmt_bind_param($stmt, "ss", $token, $currentDate);
        mysqli_stmt_execute($stmt);

        $result = mysqli_stmt_get_result($stmt);

        if (!$row = mysqli_fetch_assoc($result))
        {
            header ("Location: /driver/create-new-password.php?error=badrq");
        exit();
        }
        else
        {
            $userEmail = $row['pwdResetEmail'];

            $sql = "UPDATE drivers SET pwd = ? WHERE email_adr = ?";

                        $stmt = mysqli_stmt_init($conn);
                        if (!mysqli_stmt_prepare($stmt, $sql))
                        {
                            header ("Location: /driver/create-new-password.php?error=stmterr");
                            exit();
                        }
                        else
                        {
                            $newPwdHash = password_hash($pwd, PASSWORD_DEFAULT);

                            mysqli_stmt_bind_param($stmt, "ss", $newPwdHash, $userEmail);
                            mysqli_stmt_execute($stmt);

                            $sql = "DELETE FROM pwdreset WHERE pwdResetEmail = ?";

                            $stmt = mysqli_stmt_init($conn);
                            if (!mysqli_stmt_prepare($stmt, $sql))
                            {
                                header ("Location: /driver/create-new-password.php?error=stmterr");
                                exit();
                            }
                            else
                            {
                                mysqli_stmt_bind_param($stmt, "s", $userEmail);
                                mysqli_stmt_execute($stmt);
                                header ("Location: /driver/driver-signin.php?error=success");
                            }
                        }
        }

    }
    mysqli_close($conn);
}

else
{
    header ("Location: /driver/password-reset.php");
}
?>