<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket Plus">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/bracketplus">
    <meta property="og:title" content="Bracket Plus">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/bracketplus/img/bracketplus-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>SOTA FREIGHTS - SHIPPERS SIGN-UP</title>

    <!-- vendor css -->
    <link href="../lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="../lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="../css/bracket.css">
    <link rel="stylesheet" href="../css/local.css">
  </head>

  <body>

    <div class="row no-gutters flex-row-reverse ht-100v">
      <div class="col-md-6 bg-gray-200 d-flex align-items-center justify-content-center">
        <div class="login-wrapper wd-250 wd-xl-350 mg-y-30">
          <h4 class="tx-inverse tx-center">Sign Up</h4>
          <p class="tx-center mg-b-60">Create your own Shipper account.</p>

          <?php
          if (isset($_GET['error']))
          {
              if($_GET['error'] == 'pnmt')
              {
                  echo '<h3 class="blinking tx-inverse tx-center">Password field does not match!</h3>';
                  echo '<h6 class="tx-inverse tx-center">Please fill up the boxes in Red</h6>';
                  if (isset($_GET['val1']))
                  {
                    $val1 = $_GET['val1'];
                  }
                  if (isset($_GET['val2']))
                  {
                    $val2 = $_GET['val2'];
                  }
                  if (isset($_GET['val3']))
                  {
                    $val3 = $_GET['val3'];
                  }
                  if (isset($_GET['val4']))
                  {
                    $val4 = $_GET['val4'];
                  }
                  if (isset($_GET['val5']))
                  {
                    $val5 = $_GET['val5'];
                  }
                  if (isset($_GET['val6']))
                  {
                    $val6 = $_GET['val6'];
                  }
                  if (isset($_GET['val7']))
                  {
                    $val7 = $_GET['val7'];
                  }
                  if (isset($_GET['val8']))
                  {
                    $val8 = $_GET['val8'];
                  }
              }
              if($_GET['error'] == 'uide')
              {
                  echo '<h3 class="blinking tx-inverse tx-center">User ID or Email Address taken!</h3>';
                  echo '<h6 class="tx-inverse tx-center">Please fill up the boxes in Red</h6>';
                  if (isset($_GET['val1']))
                  {
                    $val1 = $_GET['val1'];
                  }
                  if (isset($_GET['val2']))
                  {
                    $val2 = $_GET['val2'];
                  }
                  if (isset($_GET['val3']))
                  {
                    $val3 = $_GET['val3'];
                  }
                  if (isset($_GET['val4']))
                  {
                    $val4 = $_GET['val4'];
                  }
                  if (isset($_GET['val5']))
                  {
                    $val5 = $_GET['val5'];
                  }
                  if (isset($_GET['val6']))
                  {
                    $val6 = $_GET['val6'];
                  }
                  if (isset($_GET['val7']))
                  {
                    $val7 = $_GET['val7'];
                  }
                  if (isset($_GET['val8']))
                  {
                    $val8 = $_GET['val8'];
                  }
              }
              if($_GET['error'] == 'stmtfailed')
              {
                  echo '<h6 class="tx-inverse tx-center">Connection Error, Please Try again!</h6>';
              }
              if($_GET['error'] == 'none')
              {
                  echo '<h2 class="blinking tx-center">Account Created Successfully!</h2>';
                  echo '<h5 class=" tx-center">Please <a href = "shipper-signin.php">SIGN-IN</a> to start shipping with us</h5>'; 
              }
          }

          
          ?>
          <form action="/includes/shipper-signup.inc.php" method="POST">
          <div class="form-group">
         
            <input type="text" class="form-control" name="shipper_id" value = "<?php echo '<p style="color:red;"> $val1; </p>' ?>" placeholder ="Enter your Username" required>
          </div><!-- form-group -->
          <div class="form-group">
            <input type="text" class="form-control" name="full_name" value = "<?php echo $val2;  ?>" placeholder="Enter your Fullname" required>
          </div><!-- form-group -->
          <div class="form-group">
            <input type="email" class="form-control" name="email_adr" value = "<?php echo $val3;  ?>" placeholder="Enter your email" required>
          </div><!-- form-group -->
          <div class="form-group">
            <input type="text" class="form-control" name="tel_num" value = "<?php echo $val4;  ?>" placeholder="Enter your Phone Number" required>
          </div><!-- form-group -->
          <div class="form-group">
            <input type="text" class="form-control" name="comp_name" value = "<?php echo $val5;  ?>" placeholder="Enter Your Company Name" required>
          </div><!-- form-group -->
          <div class="form-group">
            <input type="text" class="form-control" name="comp_adr" value = "<?php echo $val6;  ?>" placeholder="Enter Your Company Address" required>
          </div><!-- form-group -->
          <div class="form-group">
            <input type="text" class="form-control" name="zipcode" value = "<?php echo $val7;  ?>" placeholder="Enter Your Zipcode" required>
          </div><!-- form-group -->
          <div class="form-group">
            <input type="text" class="form-control" name="tax_id" value = "<?php echo $val8;  ?>" placeholder="Enter Your Tax ID" required>
          </div><!-- form-group -->
          <div class="form-group">
            <input style="border-color: red;" type="password" class="form-control" name="pwd" minlength="8" placeholder="Enter your password" required>
          </div><!-- form-group -->
          <div class="form-group">
            <input style="border-color: red;" type="password" class="form-control" name="pwdrepeat" minlength="8" placeholder="Repeat password" required>
          </div><!-- form-group -->
          <div class="form-group tx-12">By clicking the Sign Up button below you agreed to our privacy policy and terms of use of our website.</div>
          <button type="submit" class="btn btn-info btn-block" name="submit">Sign Up</button>
          </form>
          <div class="mg-t-60 tx-center">Already a member? <a href="shipper-signin.php" class="tx-info">Sign In</a></div>
        </div><!-- login-wrapper -->
      </div><!-- col -->
      <div class="col-md-6 bg-br-primary d-flex align-items-center justify-content-center">
        <div class="wd-250 wd-xl-450 mg-y-30">
        <div class="wd-250 wd-xl-450 mg-y-30">
          <div class="signin-logo tx-28 tx-bold tx-white"><img src="../img/logo21.png" alt="Truckload analysis Chat"></div>
          <div class="tx-white mg-b-60"><h6>Freight Services For Perfectionist</h6></div>

          <h5 class="tx-white">Why SOTA Freights?</h5>
          <p class="tx-white-6">We offer distinct Professional Freights services, quick response time, well maintained trucks and highly trained drivers.</p>
          <p class="tx-white-6 mg-b-60">We are focused on building long lasting business relationship with our customers through quality service delivery and customer satisfaction.</p>
          <a href="https://sotafreight.com/" class="btn btn-outline-light bd bd-white bd-2 tx-white pd-x-25 tx-uppercase tx-12 tx-spacing-2 tx-medium">SOTA FREIGHT-Home</a>
        </div><!-- wd-500 -->
      </div>
    </div><!-- row -->

    <script src="../lib/jquery/jquery.min.js"></script>
    <script src="../lib/jquery-ui/ui/widgets/datepicker.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../lib/select2/js/select2.min.js"></script>
    <script>
      $(function(){
        'use strict';

        $('.select2').select2({
          minimumResultsForSearch: Infinity
        });
      });
    </script>

  </body>
</html>
