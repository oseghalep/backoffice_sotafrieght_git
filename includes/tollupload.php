<?php 
//Database Connection
require_once 'dbconn.inc.php';

//Generate Random String

function generateRandomString($length = 5) 
{
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

if(isset($_POST["upload"]))
{
       
          $file = $_FILES['file']['tmp_name'];
          $handle = fopen($file, "r");
          $c = 0;
while(($filesop = fgetcsv($handle, 1000, ",")) !== false)
{
    $comp_name = $_POST['upload_comp_name'];
    $truck_id = $_POST['upload_truck_id'];
    $driver_name = $_POST['upload_driver_name'];

$trans = $filesop[0];
$transponder = $filesop[1];
$toll_date = $filesop[2];
$toll_time = $filesop[3];
$amt = $filesop[4];

$randgen = generateRandomString();
$toll_id = "TG".$randgen;
$chktoll = "SELECT * FROM toll WHERE toll_id = '$toll_id'";
$output = $conn->query($chktoll);
   
if (mysqli_num_rows($output) > 0)
{    
    mysqli_stmt_close($output);
    header("Location: /admin/create-tollrec.php?error=stmtfailure");
    exit();
}

$sql = "INSERT INTO toll (toll_id, comp_name, truck_id, driver_name, trans, transponder, toll_date, toll_time, amt) VALUE (?,?,?,?,?,?,?,?,?);";
    $stmt = mysqli_stmt_init($conn);
    
    if (!mysqli_stmt_prepare($stmt, $sql))
    {
        header("Location: /admin/create-tollrec.php?error=stmtfailed");
        exit();
    }

        mysqli_stmt_bind_param($stmt, "sssssssss", $toll_id, $comp_name, $truck_id, $driver_name, $trans, $transponder, $toll_date, $toll_time, $amt);

        mysqli_stmt_execute($stmt);
        $c = $c + 1;
}
        mysqli_stmt_close($stmt);
        header("Location: /admin/create-tollrec.php?error=uploaded");
        exit();
           


}
?>