<?php
require_once 'config.inc.php';
require 'dbconn.inc.php';
?>
<?php
if (isset($_POST['submit']))
{
    $comp_name = $_POST['comp_name'];
    $reg_num = $_POST['reg_num'];
    $truck_make = $_POST['truck_make'];
    $truck_model = $_POST['truck_model'];
    $truck_eng_type = $_POST['truck_eng_type'];
    $trailer_size = $_POST['trailer_size'];
    $prod_year = $_POST['prod_year'];
    $truck_state = $_POST['truck_state'];
    $truck_city = $_POST['truck_city'];   

    
    createtruck($conn, $comp_name, $reg_num, $truck_make, $truck_model, $truck_eng_type, $trailer_size, $prod_year, $truck_state, $truck_city);
}
else
{
    header("Location: /admin/truck-register.php");
    exit();
}
 
?>
