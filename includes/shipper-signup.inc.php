<?php
require_once 'config.ship.inc.php';
require 'dbconn.inc.php';
?>
<?php
if (isset($_POST['submit']))
{
    $shipper_id = $_POST['shipper_id'];
    $full_name = $_POST['full_name'];
    $email_adr = $_POST['email_adr'];
    $tel_num = $_POST['tel_num'];
    $comp_name = $_POST['comp_name'];
    $comp_adr = $_POST['comp_adr'];
    $zipcode = $_POST['zipcode'];
    $tax_id = $_POST['tax_id'];
    $pwd = $_POST['pwd'];
    $pwdrepeat = $_POST['pwdrepeat'];   


    if (pwdMatch($pwd, $pwdrepeat) !== false)
    {
        header("Location: /ship/shipper-signup.php?error=pnmt" ."&". "val1=$shipper_id" ."&". "val2=$full_name" ."&". "val3=$email_adr" ."&". "val4=$tel_num" ."&". "val5=$comp_name" ."&". "val6=$comp_adr" ."&". "val7=$zipcode" ."&". "val8=$tax_id");
        exit();
    }

    if (SidExists($conn, $shipper_id, $email_adr) !== false)
    {
        header("Location: /ship/shipper-signup.php?error=uide" ."&". "val1=$shipper_id" ."&". "val2=$full_name" ."&". "val3=$email_adr" ."&". "val4=$tel_num" ."&". "val5=$comp_name" ."&". "val6=$comp_adr" ."&". "val7=$zipcode" ."&". "val8=$tax_id");
        exit();  
    }

    createShipper($conn, $shipper_id, $full_name, $email_adr, $tel_num, $comp_name, $comp_adr, $zipcode, $tax_id, $pwd);
}
else
{
    header("Location: /ship/shipper-signup.php");
}

