<?php

if (isset ($_POST['login'])) 
{
    
    $adminid = $_POST["adminid"];
    $adminpwd = $_POST["adminpwd"];

    require_once 'config.inc.php';
    require_once 'dbconn.inc.php';

    if (emptyInputAdmin($adminid, $adminpwd) !== false)
    {
        header("Location: /admin/signin-split.php?error=emptyinput");
        exit();
    }
    
    loginAdmin($conn, $adminid, $adminpwd); 
}
    else
    {
        header("Location: /admin/signin-split.php");
        exit();
    }

    