<?php

if (isset($_POST["pwd_reset"]))
{
    $userEmail = $_POST["email"];
    if (empty ($userEmail))
    {
        header("Location: /ship/password-reset.php?error=emptyinput");
        exit();  
    }

    require 'dbconn.inc.php';

    $sql = "SELECT * FROM shippers WHERE email_adr = ?";

    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql))
    {
        header("Location: /ship/password-reset.php?error=stmterr");
        exit();
    }
    else
    {
    mysqli_stmt_bind_param($stmt, "s", $userEmail);
    mysqli_stmt_execute($stmt);


    $result = mysqli_stmt_get_result($stmt);
    }
    if (!$row = mysqli_fetch_assoc($result))
    {
        header("Location: /ship/password-reset.php?error=nousr");
        exit();
    }
    else
    {
            function generateRandomString($length = 32) 
            {
                $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $charactersLength = strlen($characters);
                $randomString = '';
                for ($i = 0; $i < $length; $i++) {
                    $randomString .= $characters[rand(0, $charactersLength - 1)];
                }
                return $randomString;
            }
            $token = generateRandomString();
            

            $url = "https://backoffice.sotafreight.com/ship/create-new-password.php?token=" . $token;

            $expires = date("U") + 1800;

            $sql = "DELETE FROM pwdreset WHERE pwdResetEmail = ?";

            $stmt = mysqli_stmt_init($conn);
            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                header("Location: /ship/password-reset.php?error=stmterr");
                exit();
            }
            else
            {
                mysqli_stmt_bind_param($stmt, "s", $userEmail);
                mysqli_stmt_execute($stmt);
            }

            $sql = "INSERT INTO pwdreset (pwdResetEmail, pwdResetToken, pwdResetExpires)  VALUES (?,?,?);";

            if (!mysqli_stmt_prepare($stmt, $sql))
            {
                header("Location: /ship/password-reset.php?error=stmterr");
                exit();
            }
            else
            {
                
                mysqli_stmt_bind_param($stmt, "sss", $userEmail, $token, $expires);
                mysqli_stmt_execute($stmt);

                mysqli_stmt_close($stmt);
                mysqli_close($conn);

                $recipient = "<" . $userEmail . ">";

                $subject = 'Password Reset for SOTA Freight';

                $message = '<p>Hello . $userEmail</p>';
                $message .= '<p>A request has been made to reset your password at SOTA Freight LTD</p>';
                $message .= '<p> Please Find your Password reset link below: </p>';
                $message .= '<p><a href="' . $url . '">' . $url . '</a></p>';

                $headers = "From: SOTA Freight <no-reply@sotafreight.com\r\n";
                $headers .= "Content-type: text/html\r\n";

                mail($recipient, $subject, $message, $headers);

                header("Location: /ship/password-reset.php?error=none");
                exit();
            }
    }
}
else
{
    header ("Location: /ship/password-reset.php");
}