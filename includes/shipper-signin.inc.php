<?php

if (isset ($_POST['login'])) 
{
    
    $shipper_id = $_POST["shipper_id"];
    $shipper_pwd = $_POST["shipper_pwd"];

    require_once 'config.ship.inc.php';
    require_once 'dbconn.inc.php';

    if (emptyInputshiplog($shipper_id, $shipper_pwd) !== false)
    {
        header("Location: /ship/shipper-signin.php?error=emptyinput");
        exit();
    }
    
    loginShipper($conn, $shipper_id, $shipper_pwd); 
}
    else
    {
        header("Location: /ship/shipper-signin.php");
        exit();
    }