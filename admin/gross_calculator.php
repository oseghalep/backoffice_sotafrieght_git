<?php
$from = date('Y-m-d', strtotime('-7 days')); 
$to = date('Y-m-d');
require_once 'dbconn.inc.php';

//Truck Load Gross
$sql = "SELECT * FROM truckload WHERE dtstamp between adddate(now(),-7) and now()";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$loadsum = 0;
while($row = $result->fetch_assoc()) 
{
  $loadsum += $row["paid_rate"];
}
}

//Driver Settlement Gross
$sql = "SELECT * FROM driver_settlement WHERE dtstamp between adddate(now(),-7) and now()";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$driver_pay = 0;
while($row = $result->fetch_assoc()) 
{
  $driver_pay += $row["driver_pay"];
}
}

//Expense Gross
$sql = "SELECT * FROM expense WHERE dtstamp between adddate(now(),-7) and now()";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$exp_amt = 0;
while($row = $result->fetch_assoc()) 
{
  $exp_amt += $row["exp_amt"];
}
}

//Fuel Gross
$sql = "SELECT * FROM fuel WHERE dtstamp between adddate(now(),-7) and now()";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$fuel_amt = 0;
while($row = $result->fetch_assoc()) 
{
  $fuel_amt += $row["amt"];
}
}

//Maintenance Gross
$sql = "SELECT * FROM maintenance WHERE dtstamp between adddate(now(),-7) and now()";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$maint_amt = 0;
while($row = $result->fetch_assoc()) 
{
  $maint_amt += $row["maint_amt"];
}
}

//Maintenance Gross
$sql = "SELECT * FROM toll WHERE date_posted between adddate(now(),-7) and now()";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$toll_amt = 0;
while($row = $result->fetch_assoc()) 
{
  $toll_amt += $row["amt"];
}
}

//Total Number of Owner Operator
$sql = "SELECT * FROM owneroperator";
$result = $conn->query($sql);
if ($result->num_rows > 0) 
{
$ooprec = mysqli_num_rows($result);
}

//Total Number of Trucks
$sql = "SELECT * FROM truck";
$result = $conn->query($sql);
if ($result->num_rows > 0) 
{
$truckrec = mysqli_num_rows($result);
}



?>