<?php
session_start();
if (isset($_SESSION["adminid"]))   
{
    $role = $_SESSION["role"];
    $email = $_SESSION["email"];
    $full_name = $_SESSION["full_name"];
    $adminid = $_SESSION["adminid"];

    if ($role === 'administrator')
    {
    ?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>SOTA FREIGHTS LTD</title>

    <!-- vendor css -->
    <link href="../lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
		<link href="../lib/ionicons/css/ionicons.min.css" rel="stylesheet">
		<link href="../lib/highlightjs/styles/github.css" rel="stylesheet">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet">
    <link href="../lib/datatables.net-dt/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="../lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="../css/bracket.css">
    <link rel="stylesheet" href="../css/local.css">

  </head>

  <body>

  <?php require 'adminmenu.php'; ?>

    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      
      <div class="br-pagetitle">
        <i class="icon icon ion-ios-bookmarks-outline"></i>
        <div>
          <h4>Truck Performance Report System</h4>
          <p class="mg-b-0">SOTA FREIGHTS ADMINISTRATOR</p>
        </div>
      </div><!-- d-flex -->


      <div class="br-pagebody">
        <div class="br-section-wrapper">

          <h6 class="br-section-label">Truck Performance Report</h6>          
          <div class="form-layout form-layout-1">
            <form method="POST" action = "">
            <div class="row mg-b-25">

<!-- GROSS CALCULATOR -->

<?php
require 'dbconn.inc.php';
if (isset($_POST['submit']))
{
$truck_id = $_POST['truck_id'];
$date_start = $_POST['date_start'];
$date_end = $_POST['date_end'];

$from = date('Y-m-d', strtotime('-7 days')); 
$to = date('Y-m-d');

//Truck Load Gross
$sql = "SELECT * FROM truckload WHERE truck_id = '$truck_id' AND pickup_date between '$date_start' and '$date_end'";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$loadsum = 0;
while($row = $result->fetch_assoc()) 
{
  $loadsum += $row["paid_rate"];
}
}

//Driver Settlement Gross
$sql = "SELECT * FROM expense WHERE truck_id = '$truck_id' AND exp_type = 'Driver Pay' AND exp_date between '$date_start' and '$date_end'";

$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$drfee = 0;
while($row = $result->fetch_assoc()) 
{
  $drfee += $row["exp_amt"];
}
}

//Maintenance Gross
$sql = "SELECT * FROM maintenance WHERE truck_id = '$truck_id' AND maint_date between '$date_start' and '$date_end'";

$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$maint_amt = 0;
while($row = $result->fetch_assoc()) 
{
  $maint_amt += $row["maint_amt"];
}
}

//Fuel Gross
$sql = "SELECT * FROM fuel WHERE truck_id = '$truck_id' AND trans_date between '$date_start' and '$date_end'";

$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$fuel_amt = 0;
while($row = $result->fetch_assoc()) 
{
  $fuel_amt += $row["amt"];
}
}

//3% Fuel Charge Expense
$sql = "SELECT * FROM expense WHERE truck_id = '$truck_id' AND exp_type = '3% Fuel Charge' AND exp_date between '$date_start' and '$date_end'";

$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$fuelcharge = 0;
while($row = $result->fetch_assoc()) 
{
  $fuelcharge += $row["exp_amt"];
}
}

//Management Fee
$sql = "SELECT * FROM expense WHERE truck_id = '$truck_id' AND exp_type = 'Management Fee' AND exp_date between '$date_start' and '$date_end'";

$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$mgtfee = 0;
while($row = $result->fetch_assoc()) 
{
  $mgtfee += $row["exp_amt"];
}
}

//Misc. Expense
$sql = "SELECT * FROM expense WHERE truck_id = '$truck_id' AND exp_type = 'Misc. Expense' AND exp_date between '$date_start' and '$date_end'";

$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$miscexp = 0;
while($row = $result->fetch_assoc()) 
{
  $miscexp += $row["exp_amt"];
}
}

//US DOT ELD Device Compliance Monthly Expense
$sql = "SELECT * FROM expense WHERE truck_id = '$truck_id' AND exp_type = 'US DOT ELD Device Compliance Monthly' AND exp_date between '$date_start' and '$date_end'";

$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$usdot = 0;
while($row = $result->fetch_assoc()) 
{
  $usdot += $row["exp_amt"];
}
}

//Insurance Payment Monthly
$sql = "SELECT * FROM expense WHERE truck_id = '$truck_id' AND exp_type = 'Insurance Payment Monthly' AND exp_date between '$date_start' and '$date_end'";

$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$insurance = 0;
while($row = $result->fetch_assoc()) 
{
  $insurance += $row["exp_amt"];
}
}

//Toll Gross
$sql = "SELECT * FROM toll WHERE truck_id = '$truck_id' AND toll_date between '$date_start' and '$date_end'";

$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$toll_amt = 0;
while($row = $result->fetch_assoc()) 
{
  $toll_amt += $row["amt"];
}
}

//Parking Fee Monthly
$sql = "SELECT * FROM expense WHERE truck_id = '$truck_id' AND exp_type = 'Parking Fee Monthly' AND exp_date between '$date_start' and '$date_end'";

$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$packingfee = 0;
while($row = $result->fetch_assoc()) 
{
  $packingfee += $row["exp_amt"];
}
}

//Trailer Sales Fee Monthly
$sql = "SELECT * FROM expense WHERE truck_id = '$truck_id' AND exp_type = 'Trailer Sales Fee Monthly' AND exp_date between '$date_start' and '$date_end'";

$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$trailersales = 0;
while($row = $result->fetch_assoc()) 
{
  $trailersales += $row["exp_amt"];
}
}

//Trailer Rental Fee Monthly
$sql = "SELECT * FROM expense WHERE truck_id = '$truck_id' AND exp_type = 'Trailer Rental Fee Monthly' AND exp_date between '$date_start' and '$date_end'";

$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$trailerrent = 0;
while($row = $result->fetch_assoc()) 
{
  $trailerrent += $row["exp_amt"];
}
}

//ELD Monthly Subcription
$sql = "SELECT * FROM expense WHERE truck_id = '$truck_id' AND exp_type = 'ELD Monthly Subcription' AND exp_date between '$date_start' and '$date_end'";

$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$eld = 0;
while($row = $result->fetch_assoc()) 
{
  $eld += $row["exp_amt"];
}
}

//Other
$sql = "SELECT * FROM expense WHERE truck_id = '$truck_id' AND exp_type = 'Other' AND exp_date between '$date_start' and '$date_end'";

$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$others = 0;
while($row = $result->fetch_assoc()) 
{
  $others += $row["exp_amt"];
}
}

//Expense Gross
$exp_gross = $drfee + $maint_amt + $fuel_amt + $fuelcharge + $mgtfee + $miscexp + $usdot +$insurance +$toll_amt + $packingfee + $trailersales + $trailerrent + $eld + $others;

//Profit For Selected Period
$profit = $loadsum - $exp_gross;

//Total Number of Owner Operator
$sql = "SELECT * FROM owneroperator";
$result = $conn->query($sql);
if ($result->num_rows > 0) 
{
$ooprec = mysqli_num_rows($result);
}

//Total Number of Trucks
$sql = "SELECT * FROM truck";
$result = $conn->query($sql);
if ($result->num_rows > 0) 
{
$truckrec = mysqli_num_rows($result);
}

}
?>

<!-- GROSS CALCULATOR END -->


            <?php

$sql = "SELECT * FROM truck";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
?>
            <div class="col-lg-4">
                <div class="form-group mg-b-10-force">
                  <label class="form-control-label">Select Truck: <span class="tx-danger">*</span></label>
                  <select class="form-control select2" name="truck_id" data-placeholder="Select Truck" required>
                    <option label="Select Truck"></option>
                    <?php
while($row = $result->fetch_assoc()) 
{
?>
                    <option value="<?php echo $row["truck_id"];?>"><?php echo $row["truck_id"];?></option>
                    <?php } ?>
                  </select>
                </div>
              </div><!-- col-4 -->
              <?php   
} ?>
<div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Report Start Date: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="date" name="date_start" required>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Report End Date: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="date" name="date_end" required>
                </div>
              </div><!-- col-4 -->
<!---End of Select Owner Operator -->
              
            </div><!-- row -->
            <div class="form-layout-footer">
              <button class="btn btn-info" name="submit">Generate Report</button>
            </div><!-- form-layout-footer -->
            </form>
          </div><!-- form-layout -->                   
         
          <h6 class="br-section-label">Report Performance DataTable</h6>
          <p class="br-section-text">NOTE: Periodic Profit Value is based on Selected Truck and Dates.</p>
          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                <th class="wd-15p">Truck ID <?php echo '<p style="text-align: center; color: green;font-family: Arial, Helvetica, sans-serif; font-size: 14px">' .$truck_id.'</p>'; ?></th>
                </tr>
                <tr>
                <th class="wd-15p">Periodic Profit <?php echo '<p style="text-align: center; color: green;font-family: Arial, Helvetica, sans-serif; font-size: 14px">$'.$profit.'</p>'; ?></th>
                </tr>
                <tr>
                  <th class="wd-15p">Truck Total Gross <?php echo '<p style="text-align: center; color: green;font-family: Arial, Helvetica, sans-serif; font-size: 14px">$'.$loadsum.'</p>'; ?></th>
                  </tr>
                  <tr>
                  <th class="wd-15p">Truck Total Expense <?php echo '<p style="text-align: center; color: green;font-family: Arial, Helvetica, sans-serif; font-size: 14px">$'.$exp_gross.'</p>'; ?></th>
                  </tr>
                  <tr>
                  <th class="wd-15p">Driver Pay Expense <?php echo '<p style="text-align: center; color: green;font-family: Arial, Helvetica, sans-serif; font-size: 14px">$'.$drfee.'</p>'; ?></th>
                  </tr>
                  <tr>
                  <th class="wd-20p">Maintenance Expense <?php echo '<p style="text-align: center; color: green;font-family: Arial, Helvetica, sans-serif; font-size: 14px">$'.$maint_amt.'</p>'; ?></th>
                  </tr>
                  <tr>
                  <th class="wd-15p">Fuel Payment <?php echo '<p style="text-align: center; color: green;font-family: Arial, Helvetica, sans-serif; font-size: 14px">$'.$fuel_amt.'</p>'; ?></th>
                  </tr>
                  <tr>
                  <th class="wd-10p">3% Fuel Charge <?php echo '<p style="text-align: center; color: green;font-family: Arial, Helvetica, sans-serif; font-size: 14px">$'.$fuelcharge.'</p>'; ?></th>
                  </tr>
                  <tr>
                  <th class="wd-25p">Management Fee Expense <?php echo '<p style="text-align: center; color: green;font-family: Arial, Helvetica, sans-serif; font-size: 14px">$'.$mgtfee.'</p>'; ?></th>
                  </tr>
                  <tr>
                  <th class="wd-25p">Miscellaneous Expense <?php echo '<p style="text-align: center; color: green;font-family: Arial, Helvetica, sans-serif; font-size: 14px">$'.$miscexp.'</p>'; ?></th>
                  </tr>
                  <tr>
                  <th class="wd-25p">US DOT ELD Device Compliance Expense <?php echo '<p style="text-align: center; color: green;font-family: Arial, Helvetica, sans-serif; font-size: 14px">$'.$usdot.'</p>'; ?></th>
                  </tr>
                  <tr>
                  <th class="wd-25p">Insurance Payment Expense <?php echo '<p style="text-align: center; color: green;font-family: Arial, Helvetica, sans-serif; font-size: 14px">$'.$insurance.'</p>'; ?></th>
                  </tr>
                  <tr>
                  <th class="wd-25p">Toll Fee <?php echo '<p style="text-align: center; color: green;font-family: Arial, Helvetica, sans-serif; font-size: 14px">$'.$toll_amt.'</p>'; ?></th>
                  </tr>
                  <tr>
                  <th class="wd-25p">Parking Fee Expense <?php echo '<p style="text-align: center; color: green;font-family: Arial, Helvetica, sans-serif; font-size: 14px">$'.$packingfee.'</p>'; ?></th>
                  </tr>
                  <tr>
                  <th class="wd-25p">Trailer Sales Fee Expense <?php echo '<p style="text-align: center; color: green;font-family: Arial, Helvetica, sans-serif; font-size: 14px">$'.$trailersales.'</p>'; ?></th>
                  </tr>
                  <tr>
                  <th class="wd-25p">Trailer Rental Fee Expense <?php echo '<p style="text-align: center; color: green;font-family: Arial, Helvetica, sans-serif; font-size: 14px">$'.$trailerrent.'</p>'; ?></th>
                  </tr>
                  <tr>
                  <th class="wd-25p">ELD Subcription Expense <?php echo '<p style="text-align: center; color: green;font-family: Arial, Helvetica, sans-serif; font-size: 14px">$'.$eld.'</p>'; ?></th>
                </tr>
</thead>

            </table>

          </div><!-- table-wrapper -->

        </div><!-- br-section-wrapper -->
      </div><!-- br-pagebody -->
      <footer class="br-footer">
        <div class="footer-left">
          <div class="mg-b-2">Copyright &copy; 2020. SOTA FREIGHT LTD. All Rights Reserved.</div>
          <div>Attentively and carefully Developed by <a href="https://solucean.ng">SOLUCEAN LTD</a></div>
        </div>
        <div class="footer-right d-flex align-items-center">
          <span class="tx-uppercase mg-r-10">Share:</span>
          <a target="_blank" class="pd-x-5" href="https://facebook.com/sotafreight"><i class="fab fa-facebook tx-20"></i></a>
          <a target="_blank" class="pd-x-5" href="https://twitter.com/sotafreight"><i class="fab fa-twitter tx-20"></i></a>
        </div>
      </footer>
    </div><!-- br-mainpanel -->
    <!-- ########## END: MAIN PANEL ########## -->

    <script src="../lib/jquery/jquery.min.js"></script>
    <script src="../lib/jquery-ui/ui/widgets/datepicker.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="../lib/moment/min/moment.min.js"></script>
    <script src="../lib/peity/jquery.peity.min.js"></script>
    <script src="../lib/highlightjs/highlight.pack.min.js"></script>
    <script src="../lib/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../lib/datatables.net-dt/js/dataTables.dataTables.min.js"></script>
    <script src="../lib/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js"></script>
    <script src="../lib/select2/js/select2.min.js"></script>

    <script src="../js/bracket.js"></script>
    <script>
      $(function(){
        'use strict';

        $('#datatable1').DataTable({
          responsive: true,
          language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page',
          }
        });

        $('#datatable2').DataTable({
          bLengthChange: false,
          searching: false,
          responsive: true
        });

        // Select2
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

      });
    </script>
  </body>
</html>


<?php
    exit();
    }
        elseif($role !== "administrator")
        {
            header("Location: /page-not-found.html");
            exit();
        }
}

if (!isset($_SESSION["adminid"]))
{
    header("Location: Location: /admin/signin-split.php");
        exit();
}
?>