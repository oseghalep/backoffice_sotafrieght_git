<?php
require 'dbconn.inc.php';

$truck_id = $_POST['truck_id'];

$from = date('Y-m-d', strtotime('-7 days')); 
$to = date('Y-m-d');

//Truck Load Gross
$sql = "SELECT * FROM truckload WHERE truck_id = '$truck_id' AND pickup_date between adddate(now(),-30) and now()";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$loadsum = 0;
while($row = $result->fetch_assoc()) 
{
  $loadsum += $row["paid_rate"];
}
}

//Driver Settlement Gross
$sql = "SELECT * FROM expense WHERE exp_type = 'Driver Pay' AND dtstamp between adddate(now(),-30) and now()";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$driver_pay = 0;
while($row = $result->fetch_assoc()) 
{
  $driver_pay += $row["exp_amt"];
}
}

//Maintenance Gross
$sql = "SELECT * FROM maintenance WHERE dtstamp between adddate(now(),-7) and now()";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$maint_amt = 0;
while($row = $result->fetch_assoc()) 
{
  $maint_amt += $row["maint_amt"];
}
}

//Fuel Gross
$sql = "SELECT * FROM fuel WHERE dtstamp between adddate(now(),-7) and now()";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$fuel_amt = 0;
while($row = $result->fetch_assoc()) 
{
  $fuel_amt += $row["amt"];
}
}

//3% Fuel Charge Expense
$sql = "SELECT * FROM expense WHERE exp_type = '3% Fuel Charge' AND dtstamp between adddate(now(),-7) and now()";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$fuelcharge = 0;
while($row = $result->fetch_assoc()) 
{
  $fuelcharge += $row["exp_amt"];
}
}

//Management Fee
$sql = "SELECT * FROM expense WHERE exp_type = 'Management Fee' AND dtstamp between adddate(now(),-7) and now()";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$mgtfee = 0;
while($row = $result->fetch_assoc()) 
{
  $mgtfee += $row["exp_amt"];
}
}

//Misc. Expense
$sql = "SELECT * FROM expense WHERE exp_type = 'Misc. Expense' AND dtstamp between adddate(now(),-7) and now()";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$miscexp = 0;
while($row = $result->fetch_assoc()) 
{
  $miscexp += $row["exp_amt"];
}
}

//US DOT ELD Device Compliance Monthly Expense
$sql = "SELECT * FROM expense WHERE exp_type = 'US DOT ELD Device Compliance Monthly' AND dtstamp between adddate(now(),-7) and now()";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$usdot = 0;
while($row = $result->fetch_assoc()) 
{
  $usdot += $row["exp_amt"];
}
}

//Insurance Payment Monthly
$sql = "SELECT * FROM expense WHERE exp_type = 'Insurance Payment Monthly' AND dtstamp between adddate(now(),-7) and now()";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$insurance = 0;
while($row = $result->fetch_assoc()) 
{
  $insurance += $row["exp_amt"];
}
}

//Toll Gross
$sql = "SELECT * FROM toll WHERE date_posted between adddate(now(),-7) and now()";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$toll_amt = 0;
while($row = $result->fetch_assoc()) 
{
  $toll_amt += $row["amt"];
}
}

//Parking Fee Monthly
$sql = "SELECT * FROM expense WHERE exp_type = 'Parking Fee Monthly' AND dtstamp between adddate(now(),-7) and now()";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$packingfee = 0;
while($row = $result->fetch_assoc()) 
{
  $packingfee += $row["exp_amt"];
}
}

//Trailer Sales Fee Monthly
$sql = "SELECT * FROM expense WHERE exp_type = 'Trailer Sales Fee Monthly' AND dtstamp between adddate(now(),-7) and now()";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$trailersales = 0;
while($row = $result->fetch_assoc()) 
{
  $trailersales += $row["exp_amt"];
}
}

//Trailer Rental Fee Monthly
$sql = "SELECT * FROM expense WHERE exp_type = 'Trailer Rental Fee Monthly' AND dtstamp between adddate(now(),-7) and now()";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$trailerrent = 0;
while($row = $result->fetch_assoc()) 
{
  $trailerrent += $row["exp_amt"];
}
}

//ELD Monthly Subcription
$sql = "SELECT * FROM expense WHERE exp_type = 'ELD Monthly Subcription' AND dtstamp between adddate(now(),-7) and now()";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$eld = 0;
while($row = $result->fetch_assoc()) 
{
  $eld += $row["exp_amt"];
}
}

//Expense Gross
$sql = "SELECT * FROM expense WHERE dtstamp between adddate(now(),-7) and now()";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$exp_amt = 0;
while($row = $result->fetch_assoc()) 
{
  $exp_amt += $row["exp_amt"];
}
}

//Total Number of Owner Operator
$sql = "SELECT * FROM owneroperator";
$result = $conn->query($sql);
if ($result->num_rows > 0) 
{
$ooprec = mysqli_num_rows($result);
}

//Total Number of Trucks
$sql = "SELECT * FROM truck";
$result = $conn->query($sql);
if ($result->num_rows > 0) 
{
$truckrec = mysqli_num_rows($result);
}


?>