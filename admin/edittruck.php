<?php
session_start();
if (isset($_SESSION["adminid"]))   
{
    $role = $_SESSION["role"];
    $email = $_SESSION["email"];
    $full_name = $_SESSION["full_name"];
    $adminid = $_SESSION["adminid"];

    if ($role === 'administrator')
    {
    ?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>SOTA FREIGHTS - Truck Update</title>

    <!-- vendor css -->
    <link href="../lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
		<link href="../lib/ionicons/css/ionicons.min.css" rel="stylesheet">
		<link href="../lib/highlightjs/styles/github.css" rel="stylesheet">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="../css/bracket.css">
    <link rel="stylesheet" href="../css/local.css">
  </head>

  <body>

  <?php require 'adminmenu.php'; ?>

    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      
      <div class="br-pagetitle">
        <i class="icon ion-ios-gear-outline"></i>
        <div>
          <h4>Truck Update</h4>
          <p class="mg-b-0">SOTA FREIGHT LTD</p>
        </div>
      </div><!-- d-flex -->

      <div class="br-pagebody">
        <div class="br-section-wrapper">

          <h6 class="br-section-label">Truck Update Form</h6>
          <p class="br-section-text">Please fill the form below.</p>

            <?php
require_once 'dbconn.inc.php';


if (isset($_POST['edit']))
{
    $truck_id = $_POST['truck_id'];

?>
    <div class="form-layout form-layout-1">
    <form method="POST" action = "/includes/edittruck.inc.php">
    <div class="row mg-b-25">
<?php
$sql = "SELECT * FROM truck WHERE truck_id = '$truck_id';";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
    while ($rows = $result->fetch_assoc())
    {


        $sql = "SELECT * FROM owneroperator";
        $result = $conn->query($sql);
        
        if ($result->num_rows > 0) 
        {
        ?>
                    <div class="col-lg-4">
                        <div class="form-group mg-b-10-force">
                          <label class="form-control-label">Select Owner Operator: <span class="tx-danger">*</span></label>
                          <select class="form-control select2" name="comp_name" data-placeholder="Choose Operator" required>
                            <option label="Choose Operator"></option>
                            <?php
        while($row = $result->fetch_assoc()) 
        {
        ?>
                            <option value="<?php echo $row["comp_name"];?>"><?php echo $row["comp_name"];?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div><!-- col-4 -->
                      <?php   
        } ?>
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label">Vehicle Registration Number: <span class="tx-danger">*</span></label>
                          <input class="form-control" type="text" name="reg_num" value="<?php echo $rows["reg_num"];?>" required>
                        </div>
                      </div><!-- col-4 -->
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label">Truck Make: <span class="tx-danger">*</span></label>
                          <input class="form-control" type="text" name="truck_make" value="<?php echo $rows["truck_make"];?>" required>
                        </div>
                      </div><!-- col-4 -->
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label">Truck Model: <span class="tx-danger">*</span></label>
                          <input class="form-control" type="text" name="truck_model" value="<?php echo $rows["truck_model"];?>" required>
                        </div>
                      </div><!-- col-4 -->
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label">Engine Type: <span class="tx-danger">*</span></label>
                          <input class="form-control" type="text" name="truck_eng_type" value="<?php echo $rows["truck_eng_type"];?>" required>
                        </div>
                      </div><!-- col-4 -->
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label">Enter Trailer Size: <span class="tx-danger">*</span></label>
                          <input class="form-control" type="text" name="trailer_size" value="<?php echo $rows["trailer_size"];?>" required>
                        </div>
                      </div><!-- col-4 -->
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label">Production Year: <span class="tx-danger">*</span></label>
                          <input class="form-control" type="text" name="prod_year" value="<?php echo $rows["prod_year"];?>" required>
                        </div>
                      </div><!-- col-4 -->
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label">State: <span class="tx-danger">*</span></label>
                          <input class="form-control" type="text" name="truck_state" value="<?php echo $rows["truck_state"];?>" required>
                        </div>
                      </div><!-- col-4 -->
                      <!-- Truck ID -->
<input type="hidden" name="truck_id" value="<?php echo $truck_id?>">
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label">City: <span class="tx-danger">*</span></label>
                          <input class="form-control" type="text" name="truck_city" value="<?php echo $rows["truck_city"];?>" required>
                        </div>
                      </div><!-- col-4 -->
 <?php 
    }  
}
else
{
    header("Location: /admin/truckrec.php");
    exit();
}
}
else
{
    header("Location: /admin/truckrec.php");
    exit();
}

?>            
            </div><!-- row -->
            <div class="form-layout-footer">
              <button class="btn btn-info" name="submit">Update Truck</button>
            </div><!-- form-layout-footer -->
            </form>
          </div><!-- form-layout -->                   
        </div><!-- br-section-wrapper -->
      </div><!-- br-pagebody -->
      <footer class="br-footer">
        <div class="footer-left">
          <div class="mg-b-2">Copyright &copy; 2020. SOTA FREIGHT LTD. All Rights Reserved.</div>
          <div>Attentively and carefully Developed by <a href="https://solucean.ng">SOLUCEAN LTD</a></div>
        </div>
        <div class="footer-right d-flex align-items-center">
          <span class="tx-uppercase mg-r-10">Share:</span>
          <a target="_blank" class="pd-x-5" href="https://facebook.com/sotafreight"><i class="fab fa-facebook tx-20"></i></a>
          <a target="_blank" class="pd-x-5" href="https://twitter.com/sotafreight"><i class="fab fa-twitter tx-20"></i></a>
        </div>
      </footer>
    </div><!-- br-mainpanel -->
    <!-- ########## END: MAIN PANEL ########## -->

    

    <script src="../lib/jquery/jquery.min.js"></script>
    <script src="../lib/jquery-ui/ui/widgets/datepicker.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="../lib/moment/min/moment.min.js"></script>
    <script src="../lib/peity/jquery.peity.min.js"></script>
    <script src="../lib/highlightjs/highlight.pack.min.js"></script>
    <script src="../lib/select2/js/select2.min.js"></script>

    <script src="../js/bracket.js"></script>
    <script>
      $(function(){
        'use strict'

        $('.form-layout .form-control').on('focusin', function(){
          $(this).closest('.form-group').addClass('form-group-active');
        });

        $('.form-layout .form-control').on('focusout', function(){
          $(this).closest('.form-group').removeClass('form-group-active');
        });

        // Select2
        $('#select2-a, #select2-b').select2({
          minimumResultsForSearch: Infinity
        });

        $('#select2-a').on('select2:opening', function (e) {
          $(this).closest('.form-group').addClass('form-group-active');
        });

        $('#select2-a').on('select2:closing', function (e) {
          $(this).closest('.form-group').removeClass('form-group-active');
        });

      });
    </script>
  </body>
</html>

<?php
    exit();
    }
        elseif($role !== "administrator")
        {
            header("Location: /page-not-found.html");
            exit();
        }
}

if (!isset($_SESSION["adminid"]))
{
    header("Location: Location: /admin/signin-split.php");
        exit();
}
?>
