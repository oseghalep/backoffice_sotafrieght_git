<?php
session_start();
if (isset($_SESSION["adminid"]))   
{
    $role = $_SESSION["role"];
    $email = $_SESSION["email"];
    $full_name = $_SESSION["full_name"];
    $adminid = $_SESSION["adminid"];

    if ($role === 'administrator')
    {
    ?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>SOTA FREIGHTS - Owner Operator Signup</title>

    <!-- vendor css -->
    <link href="../lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
		<link href="../lib/ionicons/css/ionicons.min.css" rel="stylesheet">
		<link href="../lib/highlightjs/styles/github.css" rel="stylesheet">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="../css/bracket.css">
    <link rel="stylesheet" href="../css/local.css">
  </head>

  <body>

  <?php require 'adminmenu.php'; ?>

    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      
      <div class="br-pagetitle">
        <i class="icon ion-ios-gear-outline"></i>
        <div>
          <h4>Owner Operator Signup</h4>
          <p class="mg-b-0">SOTA FREIGHT LTD</p>
        </div>
      </div><!-- d-flex -->

      <div class="br-pagebody">
        <div class="br-section-wrapper">

          <h6 class="br-section-label">Owner Operator Form</h6>
          <p class="br-section-text">Please fill the form below.</p>
          <?php 
              if (isset($_GET['error']))
              {
                  if($_GET['error'] == 'f12')
                  {
                    echo '<h5 class="blinking tx-inverse tx-center">Email Address not Available!</h5>';
                  if (isset($_GET['val2']))
                  {
                    $val2 = $_GET['val2'];
                  }
                  if (isset($_GET['val3']))
                  {
                    $val3 = $_GET['val3'];
                  }
                  if (isset($_GET['val4']))
                  {
                    $val4 = $_GET['val4'];
                  }
                  if (isset($_GET['val5']))
                  {
                    $val5 = $_GET['val5'];
                  }
                  if (isset($_GET['val6']))
                  {
                    $val6 = $_GET['val6'];
                  }
                  if (isset($_GET['val7']))
                  {
                    $val7 = $_GET['val7'];
                  }
                  if (isset($_GET['val8']))
                  {
                    $val8 = $_GET['val8'];
                  }
                  if (isset($_GET['val9']))
                  {
                    $val9 = $_GET['val9'];
                  }
                  if (isset($_GET['val10']))
                  {
                    $val10 = $_GET['val10'];
                  }
                  }
                  if($_GET['error'] == 'f1')
                  {
                    echo '<h5 class="blinking tx-inverse tx-center">Company Name not Available!</h5>';
                  if (isset($_GET['val2']))
                  {
                    $val2 = $_GET['val2'];
                  }
                  if (isset($_GET['val3']))
                  {
                    $val3 = $_GET['val3'];
                  }
                  if (isset($_GET['val4']))
                  {
                    $val4 = $_GET['val4'];
                  }
                  if (isset($_GET['val5']))
                  {
                    $val5 = $_GET['val5'];
                  }
                  if (isset($_GET['val6']))
                  {
                    $val6 = $_GET['val6'];
                  }
                  if (isset($_GET['val7']))
                  {
                    $val7 = $_GET['val7'];
                  }
                  if (isset($_GET['val8']))
                  {
                    $val8 = $_GET['val8'];
                  }
                  if (isset($_GET['val9']))
                  {
                    $val9 = $_GET['val9'];
                  }
                  if (isset($_GET['val10']))
                  {
                    $val10 = $_GET['val10'];
                  }
                  }
                  if($_GET['error'] == 'stmtfailed')
                  {
                    echo '<h5 class="blinking tx-inverse tx-center">Connection Error, Please try again!</h5>';
                  }
                  if($_GET['error'] == 'none')
                  {
                    echo '<h5 class="blinking tx-inverse tx-center">Owner Operator Account Created Successfully!</h5>';
                  }
              }
          ?>
          
          <div class="form-layout form-layout-1">
            <form method="POST" action = "/includes/owneroperatorSignup.inc.php">
            <div class="row mg-b-25">
              <div class="col-lg-4">
                <div class="form-group mg-b-10-force">
                  <label class="form-control-label">Enter Owner Operator Email: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="email" name="op_em" value="<?php echo $val2; ?>" placeholder="Owner Operator Email" required>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Enter Contact Name: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="contact_name" value="<?php echo $val3; ?>" placeholder="Contact Name" required>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Enter Company Name: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="comp_name" value="<?php echo $val4; ?>" placeholder="Company Name" required>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Owner Operator Address: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="op_adr" value="<?php echo $val5; ?>" placeholder="Enter address" required>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Owner Operator Phone Number: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="op_ph" value="<?php echo $val6; ?>" placeholder="Phone Number" required>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Owner Operator City: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="op_city" value="<?php echo $val7; ?>" placeholder="City" required>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Owner Operator State: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="op_state" value="<?php echo $val8; ?>" placeholder="State" required>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Owner Operator Zip Code: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="op_zip" value="<?php echo $val9; ?>" placeholder="Zipcode" required>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Owner Operator Tax ID: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="op_tax" value="<?php echo $val10; ?>" placeholder="Tax ID" required>
                </div>
              </div><!-- col-4 -->
            </div><!-- row -->
            <div class="form-layout-footer">
              <button class="btn btn-info" name="submit">Submit Form</button>
            </div><!-- form-layout-footer -->
            </form>
          </div><!-- form-layout -->                   
        </div><!-- br-section-wrapper -->
      </div><!-- br-pagebody -->
      <footer class="br-footer">
        <div class="footer-left">
          <div class="mg-b-2">Copyright &copy; 2020. SOTA FREIGHT LTD. All Rights Reserved.</div>
          <div>Attentively and carefully Developed by <a href="https://solucean.ng">SOLUCEAN LTD</a></div>
        </div>
        <div class="footer-right d-flex align-items-center">
          <span class="tx-uppercase mg-r-10">Share:</span>
          <a target="_blank" class="pd-x-5" href="https://facebook.com/sotafreight"><i class="fab fa-facebook tx-20"></i></a>
          <a target="_blank" class="pd-x-5" href="https://twitter.com/sotafreight"><i class="fab fa-twitter tx-20"></i></a>
        </div>
      </footer>
    </div><!-- br-mainpanel -->
    <!-- ########## END: MAIN PANEL ########## -->

    <div id="modaldemo" class="modal fade effect-scale">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content bd-0 rounded-0">
          <div class="modal-body pd-0">
            <div class="row flex-row-reverse no-gutters">
              <div class="col-lg-6">
                <div class="pd-30">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <div class="pd-xs-x-30 pd-y-10">
                    <h5 class="tx-xs-28 tx-inverse mg-b-5">Welcome back!</h5>
                    <p>Sign in to your account to continue</p>
                    <br>
                    <div class="form-group">
                      <input type="email" name="email" class="form-control pd-y-12" placeholder="Enter your email">
                    </div><!-- form-group -->
                    <div class="form-group mg-b-20">
                      <input type="email" name="password" class="form-control pd-y-12" placeholder="Enter your password">
                      <a href="" class="tx-12 d-block mg-t-10">Forgot password?</a>
                    </div><!-- form-group -->

                    <button class="btn btn-primary pd-y-12 btn-block">Sign In</button>

                    <div class="mg-t-30 mg-b-20 tx-12">Don't have an account yet? <a href="">Sign Up</a></div>
                  </div>
                </div><!-- pd-20 -->
              </div><!-- col-6 -->
              <div class="col-lg-6 bg-primary">
                <div class="pd-40">
                  <h4 class="tx-white mg-b-20"><span>[</span> bracket + <span>]</span></h4>
                  <p class="tx-white op-7 mg-b-60">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                  <p class="tx-white tx-13">
                    <span class="tx-uppercase tx-medium d-block mg-b-15">Our Address:</span>
                    <span class="op-7">Ayala Center, Cebu Business Park, Cebu City, Cebu, Philippines 6000</span>
                  </p>
                </div>
              </div><!-- col-6 -->
            </div><!-- row -->
          </div><!-- modal-body -->
        </div><!-- modal-content -->
      </div><!-- modal-dialog -->
    </div><!-- modal -->

    <script src="../lib/jquery/jquery.min.js"></script>
    <script src="../lib/jquery-ui/ui/widgets/datepicker.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="../lib/moment/min/moment.min.js"></script>
    <script src="../lib/peity/jquery.peity.min.js"></script>
    <script src="../lib/highlightjs/highlight.pack.min.js"></script>
    <script src="../lib/select2/js/select2.min.js"></script>

    <script src="../js/bracket.js"></script>
    <script>
      $(function(){
        'use strict'

        $('.form-layout .form-control').on('focusin', function(){
          $(this).closest('.form-group').addClass('form-group-active');
        });

        $('.form-layout .form-control').on('focusout', function(){
          $(this).closest('.form-group').removeClass('form-group-active');
        });

        // Select2
        $('#select2-a, #select2-b').select2({
          minimumResultsForSearch: Infinity
        });

        $('#select2-a').on('select2:opening', function (e) {
          $(this).closest('.form-group').addClass('form-group-active');
        });

        $('#select2-a').on('select2:closing', function (e) {
          $(this).closest('.form-group').removeClass('form-group-active');
        });

      });
    </script>
  </body>
</html>


<?php
    exit();
    }
        elseif($role !== "administrator")
        {
            header("Location: /page-not-found.html");
            exit();
        }
}

if (!isset($_SESSION["adminid"]))
{
    header("Location: Location: /admin/signin-split.php");
        exit();
}
?>