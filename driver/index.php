<?php
session_start();
if (isset($_SESSION["driver_id"]))   
{
    $role = $_SESSION["role"];
    $driver_id = $_SESSION["driver_id"];
    $full_name = $_SESSION["full_name"];
    $email_adr = $_SESSION["email_adr"];
    $truck_id = $_SESSION["truck_id"];

    if ($role === 'driver')
    {
    ?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="SOTA FREIGHTS LTD">
    <meta name="author" content="SOTA FREIGHTS">

    <title>SOTA FREIGHTS LTD</title>

    <!-- vendor css -->
    <link href="../lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="../lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="../lib/rickshaw/rickshaw.min.css" rel="stylesheet">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet">
    <link href="../lib/highlightjs/styles/github.css" rel="stylesheet">
    <link href="../lib/datatables.net-dt/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="../lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="../css/bracket.css">
  </head>

  <body>
  
    <?php require 'menu.php';
    
    // GROSS CALCULATOR

    $from = date('Y-m-d', strtotime('-7 days')); 
$to = date('Y-m-d');
require_once 'dbconn.inc.php';

//Truck Load Gross
$sql = "SELECT * FROM truckload WHERE driver_name = '$full_name' AND dtstamp between adddate(now(),-7) and now()";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$loadsum = 0;
while($row = $result->fetch_assoc()) 
{
  $loadsum += $row["paid_rate"];
}
}

//Driver Settlement Gross
$sql = "SELECT * FROM driver_settlement WHERE driver_name = '$full_name' AND dtstamp between adddate(now(),-7) and now()";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$driver_pay = 0;
while($row = $result->fetch_assoc()) 
{
  $driver_pay += $row["driver_pay"];
}
}

//Expense Gross
$sql = "SELECT * FROM expense WHERE driver_name = '$full_name' AND exp_type = 'Driver Pay' AND dtstamp between adddate(now(),-7) and now()";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$exp_amt = 0;
while($row = $result->fetch_assoc()) 
{
  $exp_amt += $row["exp_amt"];
}
}

//Fuel Gross
$sql = "SELECT * FROM fuel WHERE driver_name = '$full_name' AND dtstamp between adddate(now(),-7) and now()";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$fuel_amt = 0;
while($row = $result->fetch_assoc()) 
{
  $fuel_amt += $row["amt"];
}
}

//Maintenance Gross
$sql = "SELECT * FROM maintenance WHERE driver_name = '$full_name' AND dtstamp between adddate(now(),-7) and now()";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$maint_amt = 0;
while($row = $result->fetch_assoc()) 
{
  $maint_amt += $row["maint_amt"];
}
}

//Maintenance Gross
$sql = "SELECT * FROM toll WHERE driver_name = '$full_name' AND date_posted between adddate(now(),-7) and now()";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
$toll_amt = 0;
while($row = $result->fetch_assoc()) 
{
  $toll_amt += $row["amt"];
}
}

// GROSS CALCULATOR
    
    ?>


    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pagetitle">
        <i class="icon ion-ios-home-outline"></i>
        <div>
          <h4>Dashboard</h4>
          <p class="mg-b-0">Simplifying your freight & logistics needs with a personal approach.</p>
        </div>
      </div>

      <div class="br-pagebody">
        <div class="row row-sm">
        <div class="col-sm-6 col-xl-3">
            <div class="bg-info rounded overflow-hidden">
              <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                <i class="ion ion-earth tx-60 lh-0 tx-white op-7"></i>
                <div class="mg-l-20">
                  <p class="tx-10 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-10">Truck Load Gross</p>
                  <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1"><?php echo "$" . $loadsum; ?></p>
                  <span class="tx-11 tx-roboto tx-white-8"><?php echo $from . "  TO  " . $to;  ?></span>
                </div>
              </div>
              <div id="ch1" class="ht-50 tr-y-1"></div>
            </div>
          </div><!-- col-3 -->
          <div class="col-sm-6 col-xl-3 mg-t-20 mg-sm-t-0">
            <div class="bg-purple rounded overflow-hidden">
              <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                <i class="ion ion-bag tx-60 lh-0 tx-white op-7"></i>
                <div class="mg-l-20">
                  <p class="tx-10 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-10">Driver Settlement</p>
                  <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1"><?php echo "$" . $driver_pay; ?></p>
                  <span class="tx-11 tx-roboto tx-white-8"><?php echo $from . "  TO  " . $to;  ?></span>
                </div>
              </div>
              <div id="ch3" class="ht-50 tr-y-1"></div>
            </div>
          </div><!-- col-3 -->
          <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
            <div class="bg-teal rounded overflow-hidden">
              <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                <i class="ion ion-monitor tx-60 lh-0 tx-white op-7"></i>
                <div class="mg-l-20">
                  <p class="tx-10 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-10">Other Expense Gross</p>
                  <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1"><?php echo "$" . $exp_amt; ?></p>
                  <span class="tx-11 tx-roboto tx-white-8"><?php echo $from . "  TO  " . $to;  ?></span>
                </div>
              </div>
              <div id="ch2" class="ht-50 tr-y-1"></div>
            </div>
          </div><!-- col-3 -->
          <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
            <div class="bg-primary rounded overflow-hidden">
              <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                <i class="ion ion-clock tx-60 lh-0 tx-white op-7"></i>
                <div class="mg-l-20">
                 <p class="tx-10 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-10">Fuel Expense Gross</p>
                  <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1"><?php echo "$" . $fuel_amt; ?></p>
                  <span class="tx-11 tx-roboto tx-white-8"><?php echo $from . "  TO  " . $to;  ?></span>
                </div>
              </div>
              <div id="ch4" class="ht-50 tr-y-1"></div>
            </div>
          </div><!-- col-3 -->
          <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
            <div class="bg-primary rounded overflow-hidden">
              <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                <i class="ion ion-clock tx-60 lh-0 tx-white op-7"></i>
                <div class="mg-l-20">
                 <p class="tx-10 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-10">Maintenance Expense Gross</p>
                  <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1"><?php echo "$" . $maint_amt; ?></p>
                  <span class="tx-11 tx-roboto tx-white-8"><?php echo $from . "  TO  " . $to;  ?></span>
                </div>
              </div>
              <div id="ch4" class="ht-50 tr-y-1"></div>
            </div>
          </div><!-- col-3 -->
          <div class="col-sm-6 col-xl-3">
            <div class="bg-info rounded overflow-hidden">
              <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                <i class="ion ion-earth tx-60 lh-0 tx-white op-7"></i>
                <div class="mg-l-20">
                  <p class="tx-10 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-10">Toll-Gate Expense Gross</p>
                  <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1"><?php echo "$" . $toll_amt; ?></p>
                  <span class="tx-11 tx-roboto tx-white-8"><?php echo $from . "  TO  " . $to;  ?></span>
                </div>
              </div>
              <div id="ch1" class="ht-50 tr-y-1"></div>
            </div>
          </div><!-- col-3 -->
          <div class="col-sm-6 col-xl-3 mg-t-20 mg-sm-t-0">
            <div class="bg-purple rounded overflow-hidden">
              <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                <i class="ion ion-bag tx-60 lh-0 tx-white op-7"></i>
                <div class="mg-l-20">
                  <p class="tx-10 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-10">Owner Operators Registered</p>
                  <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1"><?php echo $ooprec; ?></p>
                  <span class="tx-11 tx-roboto tx-white-8">Total Number of Operators</span>
                </div>
              </div>
              <div id="ch3" class="ht-50 tr-y-1"></div>
            </div>
          </div><!-- col-3 -->
          <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
            <div class="bg-teal rounded overflow-hidden">
              <div class="pd-x-20 pd-t-20 d-flex align-items-center">
                <i class="ion ion-monitor tx-60 lh-0 tx-white op-7"></i>
                <div class="mg-l-20">
                  <p class="tx-10 tx-spacing-1 tx-mont tx-semibold tx-uppercase tx-white-8 mg-b-10">Total Number of Trucks</p>
                  <p class="tx-24 tx-white tx-lato tx-bold mg-b-0 lh-1"><?php echo $truckrec; ?></p>
                  <span class="tx-11 tx-roboto tx-white-8">Trucks in Service</span>
                </div>
              </div>
              <div id="ch2" class="ht-50 tr-y-1"></div>
            </div>
          </div><!-- col-3 -->
        </div><!-- row -->
      </div><!-- br-pagebody -->

      <!-- Truck Load Table Begins Here -->

      <div class="br-pagebody">
      <div class="br-section-wrapper">
            <h5 class="br-section-label" style="display: inline;">Load Weekly Report Summary From:</h5>
            <h6 style="display: inline;"><?php echo $from . "  TO  " . $to;  ?></h6>
              <p class="br-section-text">Searching, ordering and paging has been implemented to this table.</p>      
          
            <?php
            require_once 'dbconn.inc.php';

            $sql = "SELECT * FROM truckload WHERE driver_name = '$full_name' AND dtstamp between adddate(now(),-7) and now()";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) 
            {
            ?>
          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-15p">Load ID</th>
                  <th class="wd-15p">Truck ID</th>
                  <th class="wd-10p">Paid Rate</th>
                  <th class="wd-25p">Pickup City</th>
                  <th class="wd-20p">Pickup Date</th>
                  <th class="wd-20p">Delivery City</th>
                  <th class="wd-15p">Delivery Date</th>
                  <th class="wd-25p">Load Status</th>
                  <th class="wd-25p">Dead-End Miles</th>
                  <th class="wd-25p">Load Miles</th>
                  <th class="wd-25p">Payment Status</th>
                  <th class="wd-25p">Rate Per Mile</th>
                  <th class="wd-25p">Paid Date</th>
                  <th class="wd-25p">Comment</th>
                </tr>
              </thead>
                            <tbody>
                            <?php

                while($row = $result->fetch_assoc()) 
                {

                ?>
            <tr>
            <td><?php echo $row["load_id"];?></td>
            <td><?php echo $row["truck_id"];?></td>
              <td><?php echo $row["paid_rate"];?></td>
              <td><?php echo $row["pickup_city"];?></td>
              <td><?php echo $row["pickup_date"];?></td>
              <td><?php echo $row["delivery_city"];?></td>
              <td><?php echo $row["delivery_date"];?></td>
              <td><?php echo $row["load_status"];?></td>
              <td><?php echo $row["end_miles"];?></td>
              <td><?php echo $row["load_miles"];?></td>
              <td><?php echo $row["pay_status"];?></td>
              <td><?php echo $row["rate_per_mile"];?></td>
              <td><?php echo $row["paid_date"];?></td>
              <td><?php echo $row["comment"];?></td>
            </tr>
            <?php } ?>
              </tbody>

            </table>
              <?php
              }
              else 
              {
              echo "No Record";
              }
              ?>
          </div><!-- table-wrapper -->
        </div><!-- br-section-wrapper -->
      </div><!-- br-pagebody -->


<!-- Begin Driver Settlement Table -->

      <div class="br-pagebody">
        <div class="br-section-wrapper">
        <h5 class="br-section-label" style="display: inline;">Driver Settlement Weekly Report Summary From:</h5>
        <h6 style="display: inline;"><?php echo $from . "  TO  " . $to;  ?></h6>
          <p class="br-section-text">Searching, ordering and paging has been implemented to this table.</p>
            <?php
            require_once 'dbconn.inc.php';

            $sql = "SELECT * FROM driver_settlement WHERE driver_name = '$full_name' AND dtstamp between adddate(now(),-7) and now()";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) 
            {
            ?>
          <div class="table-wrapper">
            <table id="datatable3" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-15p">Driver Settlement ID</th>
                  <th class="wd-15p">Truck ID</th>
                  <th class="wd-15p">Load ID</th>
                  <th class="wd-20p">Driver Payment</th>
                  <th class="wd-15p">Pickup City</th>
                  <th class="wd-10p">Pickup Date</th>
                  <th class="wd-25p">Delivery City</th>
                  <th class="wd-25p">Delivery Date</th>
                  <th class="wd-25p">End Miles</th>
                  <th class="wd-25p">Load Miles</th>
                </tr>
              </thead>
                            <tbody>
                            <?php
              while($row = $result->fetch_assoc()) 
              {
              ?>
            <tr>
            <td><?php echo $row["drs_id"];?></td>
              <td><?php echo $row["truck_id"];?></td>
              <td><?php echo $row["load_id"];?></td>
              <td><?php echo $row["driver_pay"];?></td>
              <td><?php echo $row["pickup_city"];?></td>
              <td><?php echo $row["pickup_date"];?></td>
              <td><?php echo $row["del_city"];?></td>
              <td><?php echo $row["del_date"];?></td>
              <td><?php echo $row["end_miles"];?></td>
              <td><?php echo $row["load_miles"];?></td>
            <?php } ?>
              </tbody>

            </table>
              <?php   
              }
              else 
              {
              echo "No Record";
              }
              ?>
          </div><!-- table-wrapper -->

        </div><!-- br-section-wrapper -->
      </div><!-- br-pagebody -->

      <!-- Expense Table Begin -->

      <div class="br-pagebody">
        <div class="br-section-wrapper">
        <h5 class="br-section-label" style="display: inline;">Other Expense Weekly Report Summary From:</h5>
        <h6 style="display: inline;"><?php echo $from . "  TO  " . $to;  ?></h6>
          <p class="br-section-text">Searching, ordering and paging has been implemented to this table.</p>
<?php
require_once 'dbconn.inc.php';

$sql = "SELECT * FROM expense WHERE driver_name = '$full_name' AND dtstamp between adddate(now(),-7) and now()";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
?>
          <div class="table-wrapper">
            <table id="datatable4" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-15p">Expense ID</th>
                  <th class="wd-15p">Truck ID</th>
                  <th class="wd-15p">Type of Expense</th>
                  <th class="wd-10p">Expense Amount</th>
                  <th class="wd-10p">Expense Status</th>
                  <th class="wd-25p">Expense Date</th>
                  <th class="wd-25p">Comment</th>
                </tr>
</thead>
              <tbody>
              <?php
while($row = $result->fetch_assoc()) 
{
?>
            <tr>
            <td><?php echo $row["esp_id"];?></td>
              <td><?php echo $row["truck_id"];?></td>
              <td><?php echo $row["exp_type"];?></td>
              <td><?php echo $row["exp_amt"];?></td>
              <td><?php echo $row["exp_status"];?></td>
              <td><?php echo $row["exp_date"];?></td>
              <td><?php echo $row["comment"];?></td>
            </tr>
            <?php } ?>
              </tbody>

            </table>
<?php   
}
else 
{
echo "No Record";
}
?>
          </div><!-- table-wrapper -->
        </div><!-- br-section-wrapper -->
      </div><!-- br-pagebody -->



<!-- Fuel Table Begins -->
<div class="br-pagebody">
        <div class="br-section-wrapper">
        <h5 class="br-section-label" style="display: inline;">Fuel Expense Weekly Summary Report From:</h5>
        <h6 style="display: inline;"><?php echo $from . "  TO  " . $to;  ?></h6>
          <p class="br-section-text">Searching, ordering and paging has been implemented to this table.</p>
<?php
require_once 'dbconn.inc.php';

$sql = "SELECT * FROM fuel WHERE driver_name = '$full_name' AND dtstamp between adddate(now(),-7) and now()";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
?>
          <div class="table-wrapper">
            <table id="datatable5" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-15p">Fuel ID</th>
                  <th class="wd-15p">Truck ID</th>
                  <th class="wd-20p">Transaction Date</th>
                  <th class="wd-15p">Transaction Time</th>
                  <th class="wd-10p">Location State / Province</th>
                  <th class="wd-25p">Product Description</th>
                  <th class="wd-25p">Quantity</th>
                  <th class="wd-25p">Amount</th>
                  <th class="wd-25p">Location Name</th>
                  <th class="wd-25p">Location Address</th>
                  <th class="wd-25p">Price Per Unit</th>
                </tr>
</thead>
              <tbody>
              <?php
while($row = $result->fetch_assoc()) 
{
?>
            <tr>
            <td><?php echo $row["fuel_id"];?></td>
              <td><?php echo $row["truck_id"];?></td>
              <td><?php echo $row["trans_date"];?></td>
              <td><?php echo $row["trans_time"];?></td>
              <td><?php echo $row["loc_state"];?></td>
              <td><?php echo $row["product_desc"];?></td>
              <td><?php echo $row["quantity"];?></td>
              <td><?php echo $row["amt"];?></td>
              <td><?php echo $row["loc_name"];?></td>
              <td><?php echo $row["loc_adr"];?></td>
              <td><?php echo $row["price_per_unit"];?></td>
            <?php } ?>
              </tbody>

            </table>
<?php   
}
else 
{
echo "No Record";
}
?>
          </div><!-- table-wrapper -->
        </div><!-- br-section-wrapper -->
      </div><!-- br-pagebody -->


<!-- Maintenance Table Begin -->
 <div class="br-pagebody">
        <div class="br-section-wrapper">
        <h5 class="br-section-label" style="display: inline;">Maintenance Expense Weekly Summary Report From:</h5>
        <h6 style="display: inline;"><?php echo $from . "  TO  " . $to;  ?></h6>
          <p class="br-section-text">Searching, ordering and paging has been implemented to this table.</p>
<?php
require_once 'dbconn.inc.php';

$sql = "SELECT * FROM maintenance WHERE driver_name = '$full_name' AND dtstamp between adddate(now(),-7) and now()";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
?>
          <div class="table-wrapper">
            <table id="datatable6" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-15p">Maintenance ID</th>
                  <th class="wd-15p">Truck ID</th>
                  <th class="wd-15p">Maintenance Date</th>
                  <th class="wd-20p">Maintenance Amount</th>
                  <th class="wd-15p">Repair Shop Name</th>
                  <th class="wd-10p">Comment</th>
                </tr>
</thead>
              <tbody>
              <?php
while($row = $result->fetch_assoc()) 
{
?>
            <tr>
            <td><?php echo $row["maint_id"];?></td>
              <td><?php echo $row["truck_id"];?></td>
              <td><?php echo $row["maint_date"];?></td>
              <td><?php echo $row["maint_amt"];?></td>
              <td><?php echo $row["shop_name"];?></td>
              <td><?php echo $row["comment"];?></td>
            </tr>
            <?php } ?>
              </tbody>

            </table>
<?php   
}
else 
{
echo "No Record";
}
?>
          </div><!-- table-wrapper -->

        </div><!-- br-section-wrapper -->
      </div><!-- br-pagebody -->



<!-- Toll Table Begin -->

<div class="br-pagebody">
        <div class="br-section-wrapper">
        <h5 class="br-section-label" style="display: inline;">Toll Expense Weekly Summary Report From:</h5>
        <h6 style="display: inline;"><?php echo $from . "  TO  " . $to;  ?></h6>
          <p class="br-section-text">Searching, ordering and paging has been implemented to this table.</p>
<?php
require_once 'dbconn.inc.php';

$sql = "SELECT * FROM toll WHERE driver_name = '$full_name' AND date_posted between adddate(now(),-7) and now()";
$result = $conn->query($sql);

if ($result->num_rows > 0) 
{
?>
          <div class="table-wrapper">
            <table id="datatable7" class="table display responsive nowrap">
              <thead>
                <tr>
                <th class="wd-15p">Toll ID</th>
                  <th class="wd-15p">Truck ID</th>
                  <th class="wd-20p">Date Posted</th>
                  <th class="wd-15p">Transaction</th>
                  <th class="wd-10p">Transponder / Lic plate</th>
                  <th class="wd-25p">Date</th>
                  <th class="wd-25p">Time</th>
                  <th class="wd-25p">Entry Plaza</th>
                  <th class="wd-25p">Lane</th>
                  <th class="wd-25p">Toll Fee</th>
                </tr>
</thead>
              <tbody>
              <?php
while($row = $result->fetch_assoc()) 
{
?>
            <tr>
            <td><?php echo $row["toll_id"];?></td>
              <td><?php echo $row["truck_id"];?></td>
              <td><?php echo $row["date_posted"];?></td>
              <td><?php echo $row["trans"];?></td>
              <td><?php echo $row["transponder"];?></td>
              <td><?php echo $row["toll_date"];?></td>
              <td><?php echo $row["toll_time"];?></td>
              <td><?php echo $row["entry_plaza"];?></td>
              <td><?php echo $row["lane"];?></td>
              <td><?php echo $row["amt"];?></td>
            </tr>
            <?php } ?>
              </tbody>

            </table>
<?php   
}
else 
{
echo "No Record";
}
$conn->close();
?>
          </div><!-- table-wrapper -->

        </div><!-- br-section-wrapper -->
      </div><!-- br-pagebody -->

      <footer class="br-footer">
        <div class="footer-left">
          <div class="mg-b-2">Copyright &copy; 2020. SOTA FREIGHT LTD. All Rights Reserved.</div>
          <div>Attentively and carefully Developed by <a href="https://solucean.ng">SOLUCEAN LTD</a></div>
        </div>
        <div class="footer-right d-flex align-items-center">
          <span class="tx-uppercase mg-r-10">Share:</span>
          <a target="_blank" class="pd-x-5" href="https://facebook.com/sotafreight"><i class="fab fa-facebook tx-20"></i></a>
          <a target="_blank" class="pd-x-5" href="https://twitter.com/sotafreight"><i class="fab fa-twitter tx-20"></i></a>
        </div>
      </footer>
    </div><!-- br-mainpanel -->
    <!-- ########## END: MAIN PANEL ########## -->

    <script src="../lib/jquery/jquery.min.js"></script>
    <script src="../lib/jquery-ui/ui/widgets/datepicker.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="../lib/moment/min/moment.min.js"></script>
    <script src="../lib/peity/jquery.peity.min.js"></script>
    <script src="../lib/rickshaw/vendor/d3.min.js"></script>
    <script src="../lib/rickshaw/vendor/d3.layout.min.js"></script>
    <script src="../lib/highlightjs/highlight.pack.min.js"></script>
    <script src="../lib/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../lib/datatables.net-dt/js/dataTables.dataTables.min.js"></script>
    <script src="../lib/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js"></script>
    <script src="../lib/rickshaw/rickshaw.min.js"></script>
    <script src="../lib/jquery.flot/jquery.flot.js"></script>
    <script src="../lib/jquery.flot/jquery.flot.resize.js"></script>
    <script src="../lib/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="../lib/jquery-sparkline/jquery.sparkline.min.js"></script>
    <script src="../lib/echarts/echarts.min.js"></script>
    <script src="../lib/select2/js/select2.full.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyAq8o5-8Y5pudbJMJtDFzb8aHiWJufa5fg"></script>
    <script src="../lib/gmaps/gmaps.min.js"></script>

    <script src="../js/bracket.js"></script>
    <script src="../js/map.shiftworker.js"></script>
    <script src="../js/ResizeSensor.js"></script>
    <script src="../js/dashboard.js"></script>
    <script>
      $(function(){
        'use strict'

        $('#datatable1').DataTable({
          responsive: true,
          language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page',
          }
        });

        $('#datatable3').DataTable({
          responsive: true,
          language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page',
          }
        });

        $('#datatable4').DataTable({
          responsive: true,
          language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page',
          }
        });

        $('#datatable5').DataTable({
          responsive: true,
          language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page',
          }
        });

        $('#datatable6').DataTable({
          responsive: true,
          language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page',
          }
        });

        $('#datatable7').DataTable({
          responsive: true,
          language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page',
          }
        });

        $('#datatable2').DataTable({
          bLengthChange: false,
          searching: false,
          responsive: true
        });

        // Select2
        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

        // FOR DEMO ONLY
        // menu collapsed by default during first page load or refresh with screen
        // having a size between 992px and 1299px. This is intended on this page only
        // for better viewing of widgets demo.
        $(window).resize(function(){
          minimizeMenu();
        });

        minimizeMenu();

        function minimizeMenu() {
          if(window.matchMedia('(min-width: 992px)').matches && window.matchMedia('(max-width: 1299px)').matches) {
            // show only the icons and hide left menu label by default
            $('.menu-item-label,.menu-item-arrow').addClass('op-lg-0-force d-lg-none');
            $('body').addClass('collapsed-menu');
            $('.show-sub + .br-menu-sub').slideUp();
          } else if(window.matchMedia('(min-width: 1300px)').matches && !$('body').hasClass('collapsed-menu')) {
            $('.menu-item-label,.menu-item-arrow').removeClass('op-lg-0-force d-lg-none');
            $('body').removeClass('collapsed-menu');
            $('.show-sub + .br-menu-sub').slideDown();
          }
        }
      });
    </script>
  </body>
</html>


<?php
    exit();
    }
        elseif($role !== "driver")
        {
            header("Location: /driver/page-not-found.html");
            exit();
        }
}

if (!isset($_SESSION["driver_id"]))
{
    header("Location: /driver/driver-signin.php");
        exit();
}
?>