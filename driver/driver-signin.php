<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <meta name="description" content="SOTA Freights LTD.">
    <meta name="author" content="SOTA Freights">

    <title>SOTA Freights LTD</title>

    <!-- vendor css -->
    <link href="../lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="../lib/ionicons/css/ionicons.min.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="../css/bracket.css">
    <link rel="stylesheet" href="../css/local.css">
  </head>

  <body>

    <div class="row no-gutters flex-row-reverse ht-100v">
      <div class="col-md-6 bg-gray-200 d-flex align-items-center justify-content-center">
        <div class="login-wrapper wd-250 wd-xl-350 mg-y-30">
          <h4 class="tx-inverse tx-center">Sign In</h4>
          <p class="tx-center mg-b-60">Welcome back my friend! Please sign in.</p>
          <?php 
              if (isset($_GET['error']))
              {
                  if($_GET['error'] == 'emptyinput')
                  {
                      echo '<h5 class="blinking tx-inverse tx-center">Please Enter Username and Password Below!</h5>';
                  }
                  if($_GET['error'] == 'incorrectuserid')
                  {
                    echo '<h5 class="blinking tx-inverse tx-center">Incorrect Username!</h5>';
                  }
                  if($_GET['error'] == 'notact')
                  {
                    echo '<h5 class="blinking tx-inverse tx-center">Account Deactivated, Please Contact Administrator!</h5>';
                  }
                  if($_GET['error'] == 'incorrectpass')
                  {
                    echo '<h5 class="blinking tx-inverse tx-center">Incorrect Password!</h5>';
                  }
              }
          ?>
          <form action="/includes/driver-signin.inc.php" method="POST">
          <div class="form-group">
            <input type="text" name="driver_id" class="form-control" placeholder="Enter your username">
          </div><!-- form-group -->
          <div class="form-group">
            <input type="password" name="driver_pwd" class="form-control" placeholder="Enter your password">
            <a href="password-reset.php" class="tx-info tx-12 d-block mg-t-10">Forgot password?</a>
          </div><!-- form-group -->
          <button type="submit" name="login" class="btn btn-info btn-block">Sign In</button>
        </form>
         
        </div><!-- login-wrapper -->
      </div><!-- col -->
      <div class="col-md-6 bg-br-primary d-flex align-items-center justify-content-center">
        <div class="wd-250 wd-xl-450 mg-y-30">
          <div class="signin-logo tx-28 tx-bold tx-white"><img src="../img/logo21.png" alt="Truckload analysis Chat"></div>
          <div class="tx-white mg-b-60"><h6>Freight Services For Perfectionist</h6></div>

          <h5 class="tx-white">Why SOTA Freights?</h5>
          <p class="tx-white-6">We offer distinct Professional Freights services, quick response time, well maintained trucks and highly trained drivers.</p>
          <p class="tx-white-6 mg-b-60">We are focused on building long lasting business relationship with our customers through quality service delivery and customer satisfaction.</p>
          <a href="https://sotafreight.com/" class="btn btn-outline-light bd bd-white bd-2 tx-white pd-x-25 tx-uppercase tx-12 tx-spacing-2 tx-medium">SOTA FREIGHT-Home</a>
        </div><!-- wd-500 -->
      </div>
    </div><!-- row -->

    <script src="../lib/jquery/jquery.min.js"></script>
    <script src="../lib/jquery-ui/ui/widgets/datepicker.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>
</html>
