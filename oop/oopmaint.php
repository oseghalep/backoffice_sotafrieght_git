<?php
session_start();
if (isset($_SESSION["op_id"]))   
{
    $role = $_SESSION["role"];
    $op_id = $_SESSION["op_id"];
    $comp_name = $_SESSION["comp_name"];
    $op_em = $_SESSION["op_em"];

    if ($role === 'operator')
    {
      
require_once 'dbconn.inc.php';
    ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    

    <title>SOTA FREIGHT LTD</title>

    <!-- vendor css -->
    <link href="../lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
		<link href="../lib/ionicons/css/ionicons.min.css" rel="stylesheet">
		<link href="../lib/highlightjs/styles/github.css" rel="stylesheet">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet">
    <link href="../lib/datatables.net-dt/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="../lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="../css/bracket.css">


    <script src="../lib/jquery/jquery.min.js"></script>
    <script src="../lib/jquery-ui/ui/widgets/datepicker.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="../lib/moment/min/moment.min.js"></script>
    <script src="../lib/peity/jquery.peity.min.js"></script>
    <script src="../lib/highlightjs/highlight.pack.min.js"></script>
    <script src="../lib/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../lib/datatables.net-dt/js/dataTables.dataTables.min.js"></script>
    <script src="../lib/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js"></script>
    <script src="../lib/select2/js/select2.min.js"></script>

    
  </head>

  <body>

  <?php require 'operatormenu.php';?> 

    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pagetitle">
        <i class="icon icon ion-ios-bookmarks-outline"></i>
        <div>
          <h4>Owner Operator - Trucks Maintenance DataTable</h4>
          <p class="mg-b-0">SOTA FREIGHT LTD</p>
        </div>
      </div><!-- d-flex -->

 <!-- Search for all Company Trucks Begin -->      
<?php
$trucks = "SELECT * FROM truck WHERE op_id = '$op_id';";
$report = $conn->query($trucks);  
if ($report->num_rows > 0) 
{
  while($row = $report->fetch_assoc()) 
  {
    $truck_id = $row["truck_id"];
    ?>

<?php
$truckload = "SELECT * FROM maintenance WHERE truck_id = '$truck_id';";
$result = $conn->query($truckload);

if ($result->num_rows > 0) 
{
?>

    <!-- Search for all Company Trucks End -->
      <script src="../js/bracket.js"></script>
    <script>
      $(function(){
        

        $('#<?php echo $truck_id; ?>').DataTable({
          responsive: true,
          language: {
            searchPlaceholder: 'Search...',
            sSearch: '',
            lengthMenu: '_MENU_ items/page',
          }
        });

      });
    </script>

      <div class="br-pagebody">
        <div class="br-section-wrapper">
          <h6 class="br-section-label"><?php echo "Truck Load Datatable, Truck ID: ". $truck_id; ?></h6>
          <p class="br-section-text">Searching, ordering and paging functions can be used for table sorting.</p>
          <div class="table-wrapper">
            <table id="<?php echo $truck_id; ?>" class="table display responsive nowrap">
              <thead>
                <tr>
                <th class="wd-15p">Maintenance ID</th>
                  <th class="wd-15p">Maintenance Date</th>
                  <th class="wd-25p">Maintenance Amount</th>
                  <th class="wd-25p">Shop Name</th>
                  <th class="wd-15p">Comment</th>
                </tr>
              </thead>
              <tbody>
              <?php
    while($row = $result->fetch_assoc()) 
    {
    ?> 
        <tr>
            <td><?php echo $row["maint_id"];?></td>
            <td><?php echo $row["maint_date"];?></td>
              <td><?php echo $row["maint_amt"];?></td>
              <td><?php echo $row["shop_name"];?></td>
              <td><?php echo $row["comment"];?></td>
            </tr>
            <?php } ?>
              </tbody>
            </table>
          </div><!-- table-wrapper -->
        </div><!-- br-section-wrapper -->
        </div><!-- br-pagebody -->

        <?php   
}
else 
{
echo "No Load Record Found!";
}

}
}
else
{
  echo "No Truck Found!";
}
$conn->close();
?>
      <footer class="br-footer">
        <div class="footer-left">
        <div class="mg-b-2">Copyright &copy; 2020. SOTA FREIGHT LTD. All Rights Reserved.</div>
          <div>Attentively and carefully Developed by <a href="https://solucean.ng">SOLUCEAN LTD</a></div>
        </div>
        <div class="footer-right d-flex align-items-center">
          <span class="tx-uppercase mg-r-10">Share:</span>
          <a target="_blank" class="pd-x-5" href="https://facebook.com/sotafreight"><i class="fab fa-facebook tx-20"></i></a>
          <a target="_blank" class="pd-x-5" href="https://twitter.com/sotafreight"><i class="fab fa-twitter tx-20"></i></a>
        </div>
      </footer>
    </div><!-- br-mainpanel -->
    <!-- ########## END: MAIN PANEL ########## -->

   
  </body>
</html>


<?php
    exit();
    }
        elseif($role !== "operator")
        {
            header("Location: /oop/page-not-found.html");
            exit();
        }
}

if (!isset($_SESSION["op_id"]))
{
    header("Location: /oop/oop-signin.php");
        exit();
}
?>