<?php
session_start();
if (isset($_SESSION["op_id"]))   
{
    $role = $_SESSION["role"];
    $op_id = $_SESSION["op_id"];
    $comp_name = $_SESSION["comp_name"];
    $op_em = $_SESSION["op_em"];

    if ($role === 'operator')
    {
    ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>SOTA FREIGHTS - User Password Update</title>

    <!-- vendor css -->
    <link href="../lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
		<link href="../lib/ionicons/css/ionicons.min.css" rel="stylesheet">
		<link href="../lib/highlightjs/styles/github.css" rel="stylesheet">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="../css/bracket.css">
    <link rel="stylesheet" href="../css/local.css">
  </head>

  <body>

     <?php require 'operatormenu.php'; ?>
    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pagetitle">
        <i class="icon ion-ios-gear-outline"></i>
        <div>
          <h4>User Password Update</h4>
          <p class="mg-b-0">SOTA FREIGHT LTD</p>
        </div>
      </div><!-- d-flex -->
      <div class="br-pagebody">
        <div class="br-section-wrapper">
          <h6 class="br-section-label">User Password Update</h6>
          <p class="br-section-text">Please fill the form below to update user Password.</p>
          <?php 
              if (isset($_GET['error']))
              {
                if($_GET['error'] == 'emt')
                  {
                    echo '<h5 class="blinking tx-inverse tx-center">Please Enter Current and New Password!</h5>';
                  }
                  if($_GET['error'] == 'pnmt')
                  {
                    echo '<h5 class="blinking tx-inverse tx-center">New Password does not Match!</h5>';
                  }
                  if($_GET['error'] == 'incorrectpass')
                  {
                    echo '<h5 class="blinking tx-inverse tx-center">Current Password not Correct!</h5>';
                  }
                  if($_GET['error'] == 'stmtfailed')
                  {
                    echo '<h5 class="blinking tx-inverse tx-center">Connection Error, Please try again!</h5>';
                  }
                  if($_GET['error'] == 'none')
                  {
                    echo '<h5 class="blinking tx-inverse tx-center">Password Changed Sucessfully!</h5>';
                    echo '<h6 class="tx-inverse tx-center">Password Change will take effect from next Signin</h6>';
                  }
              }
          ?>
     
          <div class="form-layout form-layout-1">
            <form method="POST" action = "/includes/oop-password-change.inc.php">
            <div class="row mg-b-25">
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Enter Current Password: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="password" name="cpwd">
                </div>
              </div><!-- col-4 -->
              <input type="hidden" name="op_id" value="<?php echo $op_id;?>">
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Enter New Password: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="password" name="pwd">
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Repeat New Password: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="password" name="pwdrepeat">
                </div>
              </div><!-- col-4 -->
            </div><!-- row -->
            <div class="form-layout-footer">
              <button class="btn btn-info" name="submit">Update Password</button>
              <button class="btn btn-secondary">Cancel</button>
            </div><!-- form-layout-footer -->
            </form>
          </div><!-- form-layout -->                 
        </div><!-- br-section-wrapper -->
      </div><!-- br-pagebody -->
      <footer class="br-footer">
        <div class="footer-left">
        <div class="mg-b-2">Copyright &copy; 2020. SOTA FREIGHT LTD. All Rights Reserved.</div>
          <div>Attentively and carefully Developed by <a href="https://solucean.ng">SOLUCEAN LTD</a></div>
        </div>
        <div class="footer-right d-flex align-items-center">
          <span class="tx-uppercase mg-r-10">Share:</span>
          <a target="_blank" class="pd-x-5" href="https://www.facebook.com/sotafreight"><i class="fab fa-facebook tx-20"></i></a>
          <a target="_blank" class="pd-x-5" href="https://twitter.com/sotafreight"><i class="fab fa-twitter tx-20"></i></a>
        </div>
      </footer>
    </div><!-- br-mainpanel -->
    <!-- ########## END: MAIN PANEL ########## -->
    <script src="../lib/jquery/jquery.min.js"></script>
    <script src="../lib/jquery-ui/ui/widgets/datepicker.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="../lib/moment/min/moment.min.js"></script>
    <script src="../lib/peity/jquery.peity.min.js"></script>
    <script src="../lib/highlightjs/highlight.pack.min.js"></script>
    <script src="../lib/select2/js/select2.min.js"></script>

    <script src="../js/bracket.js"></script>
    <script>
      $(function(){
        'use strict'

        $('.form-layout .form-control').on('focusin', function(){
          $(this).closest('.form-group').addClass('form-group-active');
        });

        $('.form-layout .form-control').on('focusout', function(){
          $(this).closest('.form-group').removeClass('form-group-active');
        });

        // Select2
        $('#select2-a, #select2-b').select2({
          minimumResultsForSearch: Infinity
        });

        $('#select2-a').on('select2:opening', function (e) {
          $(this).closest('.form-group').addClass('form-group-active');
        });

        $('#select2-a').on('select2:closing', function (e) {
          $(this).closest('.form-group').removeClass('form-group-active');
        });

      });
    </script>
  </body>
</html>

<?php
    exit();
    }
        elseif($role !== "operator")
        {
            header("Location: /oop/page-not-found.html");
            exit();
        }
}

if (!isset($_SESSION["op_id"]))
{
    header("Location: /oop/oop-signin.php");
        exit();
}
?>