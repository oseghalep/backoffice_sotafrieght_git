<?php
session_start();
if (isset($_SESSION["op_id"]))   
{
    $role = $_SESSION["role"];
    $op_id = $_SESSION["op_id"];
    $comp_name = $_SESSION["comp_name"];
    $op_em = $_SESSION["op_em"];

    if ($role === 'operator')
    {
      
require_once 'dbconn.inc.php';
    ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>SOTA FREIGHTS - Truck Profile Information</title>

    <!-- vendor css -->
    <link href="../lib/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
		<link href="../lib/ionicons/css/ionicons.min.css" rel="stylesheet">
		<link href="../lib/highlightjs/styles/github.css" rel="stylesheet">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="../css/bracket.css">
  </head>

  <body>

 <?php require 'operatormenu.php';?>
    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
      <div class="br-pagetitle">
        <i class="icon ion-ios-gear-outline"></i>
        <div>
          <h4>Truck Profile Information</h4>
          <p class="mg-b-0">SOTA FREIGHT LTD</p>
        </div>
      </div><!-- d-flex -->
      <div class="br-pagebody">
        <div class="br-section-wrapper">
       <div> <!-- Table Begin -->
       <fieldset>
            <legend>Truck Profile Information</legend>
       <?php
  
$trucks = "SELECT * FROM truck WHERE op_id = '$op_id';";
$result = $conn->query($trucks);

if ($result->num_rows > 0) 
{
?>
      <div class="table-wrapper">
            <table class="table table-bordered table-primary">
        <thead>
        <tr>
                  <th class="wd-15p">Truck ID</th>
                  <th class="wd-15p">Registration Number</th>
                  <th class="wd-15p">Truck Make</th>
                  <th class="wd-20p">Truck Model</th>
                  <th class="wd-15p">Truck Engine Type</th>
                  <th class="wd-15p">Trailer Size</th>
                  <th class="wd-10p">Production Year</th>
                  <th class="wd-25p">Truck State</th>
                  <th class="wd-25p">Truck City</th>
                  <th class="wd-25p">Truck Status</th>
                </tr>
        </thead>
        <tbody>
            <?php
    while($row = $result->fetch_assoc()) 
    {
    ?> 
        <tr>
            <td><?php echo $row["truck_id"];?></td>
              <td><?php echo $row["reg_num"];?></td>
              <td><?php echo $row["truck_make"];?></td>
              <td><?php echo $row["truck_model"];?></td>
              <td><?php echo $row["truck_eng_type"];?></td>
              <td><?php echo $row["trailer_size"];?></td>
              <td><?php echo $row["prod_year"];?></td>
              <td><?php echo $row["truck_state"];?></td>
              <td><?php echo $row["truck_city"];?></td>
              <td><?php echo $row["truck_status"];?></td>
            </tr>
            <?php } ?>
        </tbody>
      </table>
      <?php   
}
else 
{
echo "No Record";
}
$conn->close();
?>
      </div><!-- table-wrapper -->
       </fieldset>
       </div><!-- Table End -->
        </div><!-- br section wrapper -->
          
      </div><!-- br-pagebody -->
      <footer class="br-footer">
        <div class="footer-left">
        <div class="mg-b-2">Copyright &copy; 2020. SOTA FREIGHT LTD. All Rights Reserved.</div>
          <div>Attentively and carefully Developed by <a href="https://solucean.ng">SOLUCEAN LTD</a></div>
        </div>
        <div class="footer-right d-flex align-items-center">
          <span class="tx-uppercase mg-r-10">Share:</span>
          <a target="_blank" class="pd-x-5" href="https://facebook.com/sotafreight"><i class="fab fa-facebook tx-20"></i></a>
          <a target="_blank" class="pd-x-5" href="https://twitter.com/sotafreight"><i class="fab fa-twitter tx-20"></i></a>
        </div>
      </footer>
    </div><!-- br-mainpanel -->
    <!-- ########## END: MAIN PANEL ########## -->

    <script src="../lib/jquery/jquery.min.js"></script>
    <script src="../lib/jquery-ui/ui/widgets/datepicker.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../lib/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="../lib/moment/min/moment.min.js"></script>
    <script src="../lib/peity/jquery.peity.min.js"></script>
    <script src="../lib/highlightjs/highlight.pack.min.js"></script>
    <script src="../lib/select2/js/select2.min.js"></script>

    <script src="../js/bracket.js"></script>
    <script>
      $(function(){
        'use strict'

        $('.form-layout .form-control').on('focusin', function(){
          $(this).closest('.form-group').addClass('form-group-active');
        });

        $('.form-layout .form-control').on('focusout', function(){
          $(this).closest('.form-group').removeClass('form-group-active');
        });

        // Select2
        $('#select2-a, #select2-b').select2({
          minimumResultsForSearch: Infinity
        });

        $('#select2-a').on('select2:opening', function (e) {
          $(this).closest('.form-group').addClass('form-group-active');
        });

        $('#select2-a').on('select2:closing', function (e) {
          $(this).closest('.form-group').removeClass('form-group-active');
        });

      });
    </script>
  </body>
</html>

<?php
    exit();
    }
        elseif($role !== "operator")
        {
            header("Location: /oop/page-not-found.html");
            exit();
        }
}

if (!isset($_SESSION["op_id"]))
{
    header("Location: /oop/oop-signin.php");
        exit();
}
?>